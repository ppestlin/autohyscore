% Function to return an echo struct according to the input parameters [might add standard for no input]

% ??berpr??fen ob Pulse existieren und sonst mit func rect erzeugen und str
% zur??ckgeben

function [echo, changed_fields] = get_hahn_echo(Hys, pulse,sweep)

clear echo;
echo.name = 'hahn_echo';
echo.sample = Hys.sample;
echo.avgs = Hys.avgs.value;
echo.reptime = Hys.myreptime.value;
echo.LO = Hys.myLO.value;
echo.nu_obs = Hys.nu_obs.value;
echo.B = Hys.b_field.value;
echo.VideoGain = Hys.VideoGain.value; %-3; % -31 to +20/22 dB (careful!)
echo.store_avgs = Hys.store_avgs.value;

pulse_str = num2cell(string(pulse));
pulse_type = {'pi2','pi'};

switch sweep
    case 'AMP sweep'
        tau = Hys.tau.echo;
        echo.shots = Hys.AMP_sweep.shots.value;
    case 'field sweep'
        tau = Hys.tau.b_sweep;
        echo.shots = Hys.b_sweep.shots.value;
    otherwise
        warning(['tau was set to echo tau as the variable sweep in get_hah_echo'...
            'was nissing or not a valid option'])
        tau = Hys.tau.echo;
end
p_exists = 1;


for pulse_number = 1:length(pulse)
    pulse_str_tmp = strcat('p_', pulse_type{pulse_number}, '_', pulse_str{pulse_number});
    if isfield(Hys, pulse_str_tmp)
        if  Hys.(pulse_str_tmp).nu_init ~= Hys.nu_obs.value
            Hys.(pulse_str_tmp).nu_init = Hys.nu_obs.value;
            changed_fields.(pulse_str_tmp) = Hys.(pulse_str_tmp);
        end
        pulse_str{pulse_number} = strcat('p_', pulse_type{pulse_number}, '_', pulse_str{pulse_number});
        echo.events{pulse_number}.pulsedef = Hys.(pulse_str{pulse_number});
    else
        p_exists = 0;
        break
    end     
end

if p_exists == 0
    for i = 1:length(pulse)
        pulse_str{i} = strcat('p_', pulse_type{i}, '_', pulse_str{i});
        pulse_creat_tmp = pulse_creator(Hys, pulse(i), pulse_type{i});
        echo.events{i}.pulsedef = pulse_creat_tmp.(pulse_str{i});
        Hys.(pulse_str{i}) = echo.events{i}.pulsedef;
        changed_fields.(pulse_str{i}) = Hys.(pulse_str{i});
    end
end
    

echo.events{1}.name = 'pi/2';
echo.events{1}.t = 0;

echo.events{2}.name = 'pi';
echo.events{2}.t = tau;

% add detection event (a silent event in this particular example)
echo.events{3}.t = 2*tau;
echo.events{3}.name = 'det';
echo.events{3}.det_len = Hys.det_len.value;


end