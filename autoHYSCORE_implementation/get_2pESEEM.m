% Function to return an echo struct according to the input parameters [might add standard for no input]

% ??berpr??fen ob Pulse existieren und sonst mit func rect erzeugen und str
% zur??ckgeben

function [echo] = get_2pESEEM(Hys, pulse_fields, tau)

clear echo;
echo.name = 'ESEEM_2P_X';
echo.sample = Hys.sample;
echo.project = Hys.project;
echo.avgs = Hys.avgs.value;
echo.reptime = Hys.myreptime.value;
echo.LO = Hys.myLO.value;
echo.nu_obs = Hys.nu_obs.value;
echo.B = Hys.b_field.value;
echo.VideoGain = Hys.VideoGain.value; %-3; % -31 to +20/22 dB (careful!)
echo.store_avgs = Hys.store_avgs.value;


echo.events{1}.t = 0;    
echo.events{1}.name = 'pi/2';
echo.events{1}.pulsedef = pulse_fields(1);

echo.events{2}.t = tau;
echo.events{2}.name = 'pi';
echo.events{2}.pulsedef = pulse_fields(2);


% add detection event (a silent event in this particular example)
echo.events{3}.t = 2*tau;
echo.events{3}.name = 'det';
echo.events{3}.det_len = Hys.ESEEM2p.det_len;

echo.shots = Hys.ESEEM2p.shots.value;

end