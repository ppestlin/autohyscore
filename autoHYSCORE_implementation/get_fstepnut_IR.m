% READY

function fstepnut = get_fstepnut_IR(Hys, pulse_fields) 

% pulse_fields is an array with the pulse fields of the hard, pi/2 and pi 
% pulse. Therefore the function canonly be used on the basis that the 
% pulses aleardy exist, not like get_hahn_echo, for example

clear fstepnut;
fstepnut.name = 'T1_Relaxation';
fstepnut.sample = Hys.sample;
fstepnut.avgs = Hys.avgs.value;
fstepnut.reptime = Hys.myreptime.value;
fstepnut.LO = Hys.myLO.value;
fstepnut.nu_obs = Hys.nu_obs.value;
fstepnut.B = Hys.b_field.value;
fstepnut.VideoGain = Hys.VideoGain.value; %-3; % -31 to +20/22 dB (careful!)
fstepnut.store_avgs = Hys.store_avgs.value;

% static sequence data

% timing
tau = Hys.tau.IR; 
obs_delay = Hys.obs_delay.IR; 

fstepnut.events{1}.t = 0; 
fstepnut.events{1}.pulsedef = pulse_fields(1);
fstepnut.events{1}.name = 'inv';

fstepnut.events{2}.t = obs_delay;
fstepnut.events{2}.pulsedef = pulse_fields(2);
fstepnut.events{2}.name = 'pi/2';

fstepnut.events{3}.t = obs_delay + tau;
fstepnut.events{3}.pulsedef = pulse_fields(3);
fstepnut.events{3}.name = 'pi';

% add detection event (a silent event in this particular example)
fstepnut.events{4}.t = obs_delay + 2*tau;
fstepnut.events{4}.name = 'det';
fstepnut.events{4}.det_len = Hys.det_len.value;

fstepnut.shots = Hys.IR.shots.value;
end