function [prov_gyro_exp,prov_b_field, changed_fields] = prov_field_sweep(Hys, pulse)

sweep_width = 500; % G 

sweep_dim = 11;
sweep_strt = Hys.b_field.value - sweep_width/2;   % G
sweep_end = Hys.b_field.value + sweep_width/2;    % G
sweep_inc = sweep_width / (sweep_dim - 1);  % G

b_vec = sweep_strt:sweep_inc:sweep_end;     % G

clear pcyc;
pcyc.name = 'o1.2';
pcyc.variables = {'events{1}.pulsedef.phase','events{3}.det_sign'};
pcyc.ax_id = 0;
pcyc.vec = [0 1; pi -1];
pcyc.reduce = 1;

[echo, changed_fields] = get_hahn_echo(Hys, pulse, 'field sweep');
echo.parvars = {pcyc};
echo.savename = '';
echo.shots = 100;
echo.VideoGain = 0; %dB, might change

measuretime = echo.reptime * echo.shots * 1e-9;

signal = zeros(1,length(b_vec));
snr = zeros(1,length(b_vec));

dta = zeros(512,11);

for i = 1:length(b_vec)
    if b_vec(i) < 0
        continue
    end

    echo.B = b_vec(i);

    % run the experiment
    launch(echo);
    
    pause(measuretime + 2);
    
    dta(:,i) = double(dig_interface('get'));
    
%     signal(i) = max(dta(:,i));
    signal(i) = sum(findpeaks(dta(:,i), 'SortStr', 'descend', 'NPeaks', 10))/10;
    noise = std(dta(round(0.9*length(dta(:,i))):end,i));
    snr(i) = signal(i)/noise;
    
end

i2Dcut(dta,1);

[~,max_ind] = max(signal);

if snr(max_ind) < 3
    error('The fieldsweep was unable to detect a propper echo. You probably need a different starting field')
end

prov_gyro_exp = (Hys.myLO.value+Hys.nu_obs.value)/b_vec(max_ind);
prov_b_field = b_vec(max_ind);

end