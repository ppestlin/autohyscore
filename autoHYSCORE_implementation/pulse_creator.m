% Pulse creater

function p_def = pulse_creator(Hys, pulse, pulse_type)

clear p_def;

pulse_str = num2cell(string(pulse));        

for i = 1 : length(pulse)
    pulse_str{i} = strcat('p_', pulse_type, '_', pulse_str{i});
    p_def.(pulse_str{i}).tp = pulse(i); 
    p_def.(pulse_str{i}).nu_init = Hys.nu_obs.value;
    p_def.(pulse_str{i}).scale =  Hys.starting_scale;
end

end




