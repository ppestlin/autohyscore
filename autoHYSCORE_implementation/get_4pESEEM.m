% Function to return an echo struct according to the input parameters [might add standard for no input]

% ??berpr??fen ob Pulse existieren und sonst mit func rect erzeugen und str
% zur??ckgeben

function [chirper] = get_4pESEEM(Hys, pulse_fields, tau, T1)

clear chirper;
chirper.name = 'Hyscore_4p_X';
chirper.sample = Hys.sample;
chirper.project = Hys.project;
chirper.avgs = Hys.avgs.value;
chirper.reptime = Hys.myreptime.value;
chirper.LO = Hys.myLO.value;
chirper.nu_obs = Hys.nu_obs.value;
chirper.B = round(Hys.b_field.value*100)/100; % DAKL: round B-field to two decimal places
chirper.VideoGain = Hys.VideoGain.value; %-3; % -31 to +20/22 dB (careful!)
chirper.store_avgs = Hys.store_avgs.value;


chirper.events{1}.t = 0;    
chirper.events{1}.name = 'pi/2';
chirper.events{1}.pulsedef = pulse_fields(1);

chirper.events{2}.t = tau;  
chirper.events{2}.name = 'pi/2';
chirper.events{2}.pulsedef = pulse_fields(2);

chirper.events{3}.t = chirper.events{2}.t + T1;
chirper.events{3}.name = 'pi';
chirper.events{3}.pulsedef = pulse_fields(3);

chirper.events{4}.t = chirper.events{3}.t + T1;
chirper.events{4}.name = 'pi/2';
chirper.events{4}.pulsedef = pulse_fields(4);


% add detection event (a silent event in this particular example)
chirper.events{5}.t = chirper.events{4}.t + chirper.events{2}.t;   
chirper.events{5}.name = 'det';
chirper.events{5}.det_len = Hys.ESEEM4p.det_len;

chirper.shots = Hys.ESEEM4p.shots.value;

end