function [tau,intensity_ratio,file] = ESEEM_2p(Hys,pulse_fields)
    
    tau = Hys.ESEEM2p.tau;
    modul_th = 0.25;
    inten_th = 0.15;
%     envelope_window = 45;
    BGtype = 'sexp';
    
    
    clear pcyc;
    pcyc.name = 'o1.2';
    pcyc.variables = {'events{1}.pulsedef.phase','events{3}.det_sign'};
    pcyc.ax_id = 0;
    pcyc.vec = [0 1; pi -1];
    pcyc.reduce = 1;
    
    clear t2_swp;
    t2_swp.name = 'tau incr';
    t2_swp.variables = {'events{2}.t', 'events{3}.t'};
    t2_swp.ax_id = 2;
    t2_swp.inc = 8.8888888*[1 2]; %1.111111111*[1 2];  %5*[1 2]; %
    t2_swp.strt = tau* [1 2];
    t2_swp.dim = 1024;
    
    echo = get_2pESEEM(Hys,pulse_fields,tau);
    swp_end = (t2_swp.strt(2) + t2_swp.inc(2) * (t2_swp.dim - 1));
    
    
    if echo.reptime < swp_end/Hys.conf_start.std.dutyX
        echo.reptime = round(1.1*swp_end/Hys.conf_start.std.dutyX,-3);
    end
    
    echo.parvars = {pcyc,t2_swp};
    echo.savename = ['2pESEEM_tau_' num2str(echo.shots) 'shots'];
    
    
    launch(echo);
    
    pause(10);
    
    while (dig_interface('savenow'))
        
        pause(5);

        desktop = com.mathworks.mde.desk.MLDesktop.getInstance;
        cmdWindow = desktop.getClient('Command Window');
        cmdWindowScrollPane = cmdWindow.getComponent(0);
        cmdWindowScrollPaneViewport = cmdWindowScrollPane.getComponent(0);
        cmdTextUI = cmdWindowScrollPaneViewport.getComponent(0);
        cmdText = cmdTextUI.getText;
        one_string = string(cmdText);
        collum_lines = regexp(one_string, '\n', 'split');
        last_string = collum_lines(end-1);
    
        displayed_data = sscanf(last_string, 'Ongoing acquisition, %i available and stored. %f to go, which will take some %f minutes');
        
        try
            if strcmp(last_string, "Acquisition is writing -> No data was saved")|| displayed_data(3) == inf
                pause(1);
                continue
            end
        catch
            pause(20);
            continue
        end
            
        time = displayed_data(3) * 60;    %seconds
    
        if time < 10
            pause(10);
        elseif time > 600
            pause(600);
        else
            pause(time);
        end 
    end
    
    file = evalin('base', 'currfilename');
    
    clear options;
    options.plot = 0; % do not plot by uwb_eval
%     options.evlen = 256; %128; %56; % enforce a 256 point evaluation window
    options.phase_all = 0; %0 for nutation, 1 for ampsweeps;

    output = uwb_eval2(file,options);

    dta_ev = output.dta_ev; % echo integrals
    ev_coll = dta_ev;
    dta_x_cont = output.dta_x(1:end); % axis of first indirect dimension

    t=(dta_x_cont{1});
    vexp=transpose(real(ev_coll));

    % baseline fitting

    BGstart = 1;

    switch BGtype
        case 'poly'
            [p,~]=polyfit(t(BGstart:end),vexp(BGstart:end),1); % fit the baseline by a high-order polynomial
            bsl=polyval(p,t); % compute the optimum baseline
        case 'sexp'
            tbg = t(BGstart:end)';
            vexpf = vexp(BGstart:end)';
            fun = @(vec,tbg,vexpf) sum(sqrt((vexpf - (vec(1).*exp(-(tbg./vec(2)).^vec(3))+vec(4))).^2)); % RMSD(data-baseline)
            fun2 = @(vec)fun(vec,tbg,vexpf);
            options = optimset('MaxFunEvals',1e8,'MaxIter',1e6);
            vec = fminsearch(fun2,[vexpf(1) 100 1 vexpf(end)],options);
            bsl = vec(1).*exp(-(t./vec(2)).^vec(3))+vec(4); % compute the optimum baseline
        case 'bisexp'
            tbg = t(BGstart:end)';
            vexpf = vexp(BGstart:end)';
            fun = @(vec,tbg,vexpf) sum(sqrt((vexpf - (vec(1).*exp(-(tbg./vec(2)).^vec(3)) + vec(4).*exp(-(tbg./vec(5)).^vec(6)) + vec(7))).^2)); % RMSD(data-baseline)
            fun2 = @(vec)fun(vec,tbg,vexpf);
            options = optimset('MaxFunEvals',1e8,'MaxIter',1e6);
            vec = fminsearch(fun2,[vexpf(1) 10 1 vexpf(1)./2 100 1 vexpf(end)],options);
            bsl = vec(1).*exp(-(t./vec(2)).^vec(3)) + vec(4).*exp(-(t./vec(5)).^vec(6)) + vec(7); % compute the optimum baseline
        otherwise
            % Standard 2pESEEM: 'sexp'
            tbg = t(BGstart:end)';
            vexpf = vexp(BGstart:end)';
            fun = @(vec,tbg,vexpf) sum(sqrt((vexpf - (vec(1).*exp(-(tbg./vec(2)).^vec(3))+vec(4))).^2)); % RMSD(data-baseline)
            fun2 = @(vec)fun(vec,tbg,vexpf);
            options = optimset('MaxFunEvals',1e8,'MaxIter',1e6);
            vec = fminsearch(fun2,[vexpf(1) 100 1 vexpf(end)],options);
            bsl = vec(1).*exp(-(t./vec(2)).^vec(3))+vec(4); % compute the optimum baseline
    end

%     [vexp_up, vexp_low] = envelope(vexp,envelope_window,'peak');

%     figure('Name','2pESEEM with fit and envelope'); clf; hold on;
%     plot(t,vexp,'g')
%     plot(t,bsl,'r')
%     plot(t,vexp_low,'b')
%     plot(t,vexp_up,'b')
%     ylabel('Re( Echo integral ) [arb]')
%     xlabel('Sweep axis')

    vexp_bsl_cor = (vexp - bsl)/max(bsl);
    
    figure('Name','2pESEEM with bsl'); clf; hold on;
    plot(t,vexp,'g')
    plot(t,bsl,'r')
    ylabel('Re( Echo integral ) [arb]')
    xlabel('Sweep axis (ns)')
    
    tau = 1/2 * t(1);

    % Check strength of modulation

    sufficient = 0;

    [~, i] = min(abs(vexp-inten_th * max(vexp)));
    i = i - 1;
    tau_max = t(i)/2;
    j = i+10;
    if j > length(t)
        j = length(t);
    end
    figure('Name','2pESEEM bsl_cor rel to bsl max'); clf; hold on;
    plot(t(1:j), vexp_bsl_cor(1:j),'b')
    ylabel('Re( Echo integral ) [arb]')
    xlabel('Sweep axis (ns)')

     while ~sufficient 
        if abs(2 * tau - t(1)) < 1
            max_dif = max(abs(vexp_bsl_cor(1:10)));
            if max_dif > modul_th
                if vec(1)*exp(-(2*(200+tau)./vec(2)).^vec(3))+vec(4) > inten_th * max(vexp)
                    tau = tau + 200;
                else
                    tau = tau_max;
                end
            else
                sufficient = 1;
            end
        else
            [~, i] = min(abs(t-2*tau));
             max_dif = max(abs(vexp_bsl_cor(i-10:i+10)));
            if max_dif > modul_th
                if abs(tau - tau_max) < 1
                    warning('The oscilation was to strong for the parameters, Tau was set to the largest allowed value');
                    sufficient = 1;
                elseif vec(1)*exp(-(2*(200+tau)./vec(2)).^vec(3))+vec(4) > inten_th * max(vexp)
                    tau = tau + 200;
                else
                    tau = tau_max;
                end   
            else
                sufficient = 1;
            end
        end
    end
    
% For envelope fnc:    
     
%     while ~sufficient 
%         if abs(tau - 2 * t(1)) < 1
%             dif = max(abs(vexp_up(1)-bsl(1)),abs(vexp_low(1)-bsl(1)));
%             if dif > modul_th * bsl(ii)
%                 if vec(1)*exp(-(2*(200+tau)./vec(2)).^vec(3))+vec(4) > inten_th * max(vexp)
%                     tau = tau + 200;
%                 else
%                     tau = tau_max;
%                 end
%             else
%                 sufficient = 1;
%             end
%         else
%             [~, i] = min(abs(t-2*tau));
%             dif = max(abs(vexp_up(i)-bsl(i)),abs(vexp_low(i)-bsl(i)));
%             if dif > modul_th * bsl(i)
%                 if abs(tau - tau_max) < 1
%                     warning('The oscilation was to strong for the parameters, Tau was set to the largest allowed value');
%                     sufficient = 1;
%                 elseif vec(1)*exp(-(2*(200+tau)./vec(2)).^vec(3))+vec(4) > inten_th * max(vexp)
%                     tau = tau + 200;
%                 else
%                     tau = tau_max;
%                 end   
%             else
%                 sufficient = 1;
%             end
%         end
%     end
    
%     plot(2*tau,vec(1)*exp(-(2*(tau)./vec(2)).^vec(3))+vec(4),'o','MarkerSize',5,'k')
    
    intensity_ratio = vec(1)*exp(-(2*(tau)./vec(2)).^vec(3))+vec(4)/max(vexp);
    if (1 - intensity_ratio) < 0.1
        intensity_ratio = 1.0;
    end
    
end