% Ready

function [Hard_length,file] = nutation(Hys,pulse_fields, clear_files)

clear pcyc;
pcyc.name = 'o1.2';
pcyc.variables = {'events{2}.pulsedef.phase','events{4}.det_sign'};
pcyc.ax_id = 0;
pcyc.vec = [0 1; pi -1];
pcyc.reduce = 1;

clear nut;
nut.name = 'nut';
nut.variables = {'events{1}.pulsedef.tp'};
nut.ax_id = 1; 
nut.inc = 1.111111;
nut.strt = 0;
nut.dim = 64;

clear fstepnut;
fstepnut = get_fstepnut_nut(Hys,pulse_fields);

fstepnut.parvars = {pcyc, nut};

fstepnut.savename = ['nutation']; % Can add additional info after pulse. Dont cahnge first two to keep structure

launch(fstepnut);

pause(5); % Pause always in seconds

while (dig_interface('savenow'))
    
    pause(1);

    desktop = com.mathworks.mde.desk.MLDesktop.getInstance;
    cmdWindow = desktop.getClient('Command Window');
    cmdWindowScrollPane = cmdWindow.getComponent(0);
    cmdWindowScrollPaneViewport = cmdWindowScrollPane.getComponent(0);
    cmdTextUI = cmdWindowScrollPaneViewport.getComponent(0);
    cmdText = cmdTextUI.getText;
    one_string = string(cmdText);
    collum_lines = regexp(one_string, '\n', 'split');
    last_string = collum_lines(end-1);

    displayed_data = sscanf(last_string, 'Ongoing acquisition, %i available and stored. %f to go, which will take some %f minutes');
    
    try
        if strcmp(last_string, "Acquisition is writing -> No data was saved")|| displayed_data(3) == inf
            pause(1);
            continue
        end
    catch
        pause(10);
        continue
    end

    time = displayed_data(3) * 60;    %seconds

    if time < 10
        pause(5);
    elseif time > 600
        pause(600);
    else
        pause(time - 5);
    end
    
end

file = evalin('base', 'currfilename');

clear options;
options.plot = 0; % do not plot by uwb_eval
options.evlen = 256; % enforce a 256 point evaluation window

options.phase_all = 0; %0 for nutation, 1 for ampsweeps;

cut=1;

dta_cont = cell(length(file),1);
dta_x_cont = dta_cont;
ev_coll = dta_cont;
exp_cont = dta_cont;


output = uwb_eval2(file,options);

dta_ev = output.dta_ev; % echo integrals

ev_coll = dta_ev;
dta_x_cont = output.dta_x{1}; % axis of first indirect dimension


figure('Name','Nutation'); clf; hold on; 

starti = 1;
    
hold on; 

plot(dta_x_cont(starti:end),real(ev_coll(starti:end))/max(real(ev_coll(starti:end))),'-','Color','r')

% legend(legendvec,'Location','best')
ylabel('Re( Echo integral ) [norm]')
xlabel('Sweep axis')

[pks,x] = findpeaks(-real(ev_coll(cut:end))/max(real(ev_coll(cut:end))));

plot(dta_x_cont(x(1)),-pks(1),'o','MarkerSize',5,'Color','blue')

Hard_length = dta_x_cont(x(1));

if ~exist('clear_files', 'var')
    clear_files = 0;
end

if clear_files == 1
    file_list = dir('*nutation*.mat');
    if length(file_list) == 1
    else
        for i = 1:(length(file_list)-1)
            delete(file_list(i).name);
        end
    end
elseif clear_files == 2
    file_list = dir('*nutation*.mat');
    file = '';
    for i = 1:(length(file_list))               
        delete(file_list(i).name);
    end
end

end