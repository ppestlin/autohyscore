% Function to return an echo struct according to the input parameters [might add standard for no input]

% ??berpr??fen ob Pulse existieren und sonst mit func rect erzeugen und str
% zur??ckgeben

function [chirper] = get_3pESEEM(Hys, pulse_fields, tau,T)

clear chirper;
chirper.name = 'ESEEM_3P_X';
chirper.sample = Hys.sample;
chirper.project = Hys.project;
chirper.avgs = Hys.avgs.value;
chirper.reptime = Hys.myreptime.value;
chirper.LO = Hys.myLO.value;
chirper.nu_obs = Hys.nu_obs.value;
chirper.B = Hys.b_field.value;
chirper.VideoGain = Hys.VideoGain.value; %-3; % -31 to +20/22 dB (careful!)
chirper.store_avgs = Hys.store_avgs.value;


chirper.events{1}.t = 0;    
chirper.events{1}.name = 'pi/2';
chirper.events{1}.pulsedef = pulse_fields(1);

chirper.events{2}.t = tau;
chirper.events{2}.name = 'pi/2';
chirper.events{2}.pulsedef = pulse_fields(2);

chirper.events{3}.t = chirper.events{2}.t + T;
chirper.events{3}.name = 'pi/2';
chirper.events{3}.pulsedef = pulse_fields(3);


% add detection event (a silent event in this particular example)
chirper.events{4}.t = chirper.events{3}.t + chirper.events{2}.t;
chirper.events{4}.name = 'det';
chirper.events{4}.det_len = Hys.ESEEM3p.det_len;

chirper.shots = Hys.ESEEM3p.shots.value;

end