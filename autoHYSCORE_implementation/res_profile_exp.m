% (ready for now)

function [fc, Q,file] = res_profile_exp(Hys,pulse_fields)
%RES_PROFILE Summary of this function goes here
%   Detailed explanation goes here

clear fsepnut;
fstepnut = get_fstepnut_RP(Hys,pulse_fields);

fstep_gyro = Hys.b_field.gyro_exp;
clear fstep; % inkrementiert nuobs nicht totale frequenz 
fstep.name = 'fstep swp';
fstep.variables = {'events{1}.pulsedef.nu_init','events{2}.pulsedef.nu_init','events{3}.pulsedef.nu_init','B'};
fstep.inc = 0.02 * [1 1 1 1/fstep_gyro]; % in GHz
fstep.strt = 1.05*[1.0 1.0 1.0]; % centered in Nyquist of new digitizer with 1 GHz swp
fstep.dim = 36; % for full Nyquist zone of digitizer
% fstep.strt = [1.3 1.3 1.3]; % centered in Nyquist of new digitizer with 1 GHz swp
% fstep.dim = 49; % for full Nyquist zone of digitizer
fstep.strt(4) = (fstep.strt(1)+fstepnut.LO)/fstep_gyro; % the magnetic field

clear nut; % sweep of pulse length of first pulse starting from 0 to 126 ns in steps of 2 ns
nut.name = 'nut';
nut.variables = {'events{1}.pulsedef.tp'};
nut.ax_id = 1; % index to be used to calculate the axis of the parameter variation (if 1 parameter vriation of first variable)
nut.inc = 2; 
nut.strt = 0;
nut.dim = 64;

clear pcyc; % phase cycling
pcyc.name = 'o1.2';
pcyc.variables = {'events{2}.pulsedef.phase','events{4}.det_sign'};
pcyc.ax_id = 0;  % index to be used to calculate the axis of the parameter variation (if 0 just enumarate from one to dimension)
pcyc.vec = [0 1; pi -1]; % default value of pulsedef.phase is 0, so phase of event{3} is always 0
pcyc.reduce = 1;  % add up all, if 0: indirect dimension

fstepnut.parvars = {pcyc, nut, fstep};

fstepnut.savename = ['fstepnut'];

launch(fstepnut);

pause(5); % Pause always in seconds

while (dig_interface('savenow'))
    
    pause(1);

    desktop = com.mathworks.mde.desk.MLDesktop.getInstance;
    cmdWindow = desktop.getClient('Command Window');
    cmdWindowScrollPane = cmdWindow.getComponent(0);
    cmdWindowScrollPaneViewport = cmdWindowScrollPane.getComponent(0);
    cmdTextUI = cmdWindowScrollPaneViewport.getComponent(0);
    cmdText = cmdTextUI.getText;
    one_string = string(cmdText);
    collum_lines = regexp(one_string, '\n', 'split');
    last_string = collum_lines(end-1);

    displayed_data = sscanf(last_string, 'Ongoing acquisition, %i available and stored. %f to go, which will take some %f minutes');
    
    try
        if strcmp(last_string, "Acquisition is writing -> No data was saved")|| displayed_data(3) == inf
            pause(1);
            continue
        end
    catch
        pause(10);
        continue
    end

    time = displayed_data(3) * 60;    %seconds

    if time < 10
        pause(5);
    elseif time > 600
        pause(600);
    else
        pause(time - 5);
    end
    
end

file = evalin('base', 'currfilename');

% Options for uwb_eval
options.phase_all = 0;
options.plot = 0;
options.ref_echo_2D_idx = 1; % enforce the use of the first point

% get downconverted data by uwb_eval
output = uwb_eval2(file,options);

dta_avg = output.dta_avg; % averaged echo transients
nut2d_dconv = output.dta_ev; % echo integrals
nut_x = output.dta_x{1}; % axis of first indirect dimension here length of first pulse
LO_freq = output.exp.LO;
nut_y = output.dta_x{2} + LO_freq; % axis of second indirect dimension here sweep of magnetic field

trace = real(nut2d_dconv);
trace = trace - repmat(mean(trace),size(trace,1),1);

[fdy2, fdx2] = getmultspec(trace,nut_x,64,2); % adapt to exp. points, window fct.

% there are some problematic traces with too low nu1 around zero-freq
nuoffs = find(fdx2 > 0.005,1);

% get the nutation frequency by taking the maximum
[~,nu1_id] = max(abs(fdy2(nuoffs:end,:)));
nu1 = abs(fdx2(nu1_id+nuoffs-1));

% lorentz-fit part

[fmax,fc_ind] = max(nu1*1e3);           % Manual maximum
fc = nut_y(fc_ind);                     
[hmpX, ~] = intersections(nut_y,nu1*1e3,nut_y,(fmax/2)*ones(length(nut_y),1)); % half maximum points

if isempty(hmpX)
    df = 0.1;                           % Random starting value to avoid errors
elseif size(hmpX)<2
    df = 2*abs(hmpX(1)-fc);             % Case of 1 intersection
else
    df = abs(hmpX(1) - hmpX(end));      % Case of min 2 intersections
end

beta = [df,fc];
model = @(beta,x)(fmax./(1 + (((x - beta(2))*2/beta(1))).^2));  % Lorentz model

idx = find(abs(nu1*1e3 - 5.00488) > 0.001);                             % Exclude values with 5 MHz (minimum signal), change for other resonator?

beta = nlinfit(nut_y(idx), nu1(idx)*1e3,model,beta);    % Fit


% Plot
figure('Name','Resonator profile'); clf; hold on; 
plot(nut_y,nu1*1e3,'color','blue')
xlabel('f [GHz]')
ylabel('\nu_1 [MHz]')
result = model(beta, nut_y);
plot(nut_y,result,'color','red')

fc = beta(2);           
Q = beta(2)/beta(1);

end

