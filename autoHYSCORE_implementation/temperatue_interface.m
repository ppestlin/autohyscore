function [output] = temperatue_interface(command,temp_value)

% Work in progress

try

switch command
    case 'set'                  % Set the temperature to the input value and return the final temperature
        output = temp_value;
    case 'get'                  % Return the current temperature
        output = temp_value;
    case 'state'                % Return 1 if temperature is stable, 0 if unstable
        output = 1;
    otherwise                   % Up for debate on what to do here
        output = -1;
end

catch                           % Catch with -1 on error
    output = -1;
end

end