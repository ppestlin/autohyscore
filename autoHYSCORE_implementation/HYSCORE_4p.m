function [is_finished] = HYSCORE_4p(Hys,pulse_fields)
    
for k = 1:length(Hys.HYSCORE.field_vec)

    Hys.b_field.value = Hys.HYSCORE.field_vec(k);
    Hys.b_field.value = round(Hys.b_field.value*100)/100; % DAKL: round to two decimal places
    tau_vec = Hys.taus;
    conf_HYSCORE = Hys.conf_4p;
    directory = [Hys.save_directory.HYSCORE '_B_' num2str(round(Hys.b_field.value)) 'G'];
    mkdir(directory);
    
    
    Hys.HYSCORE.trace_time.value = Hys.HYSCORE.shots.value * Hys.HYSCORE.T2_swp.dim *8* Hys.myreptime.value/(1e9*60);%min
    Hys.HYSCORE.measure_time.value = Hys.HYSCORE.trace_time.value * Hys.HYSCORE.T2_swp.dim;%min
    Hys.HYSCORE.measure_time.value = 1.01 * Hys.HYSCORE.measure_time.value * length(tau_vec) / 60;
    Hys.HYSCORE.measure_time.unit = 'hrs';
    
    plot_rate = round(30/Hys.HYSCORE.trace_time.value);
    if plot_rate < 1
        plot_rate = 1;
    end
    
    
    save([directory '/Hyscore_struc_4_this_HYSCORE.mat'],'Hys')
    
    
    for t = 1:length(tau_vec)

        tau = tau_vec(t);
        T1 = Hys.HYSCORE.T1_swp.strt; % same starting value         % 128 or tau; % tau + 8
        chirper = get_4pESEEM(Hys, pulse_fields, tau, T1);
        chirper.B = round(Hys.HYSCORE.field_vec(k)*100)/100;
        chirper.shots = Hys.HYSCORE.shots.value;
        chirper.savepath = ['./' directory];
        
        clear pcyc3_4p;
        pcyc3_4p.name = '8-step echo cycle for HYSCORE';
        pcyc3_4p.variables = {'events{1}.pulsedef.phase','events{4}.pulsedef.phase','events{3}.pulsedef.phase','events{5}.det_sign'};
        pcyc3_4p.vec = [0    0       0    +1; ...
                        0   pi      0    -1; ...
                        0   0       pi    +1; ...
                        0   pi      pi    -1; ...
                        pi    0       0    -1; ...
                        pi   pi      0    +1; ...
                        pi   0       pi    -1; ...
                        pi   pi      pi    +1; ...
                         ];
        pcyc3_4p.reduce = 1;

        clear T2_swp;
        T2_swp.name = 'T2 incr';
        T2_swp.variables = {'events{4}.t', 'events{5}.t'}; 
        T2_swp.inc = [Hys.HYSCORE.T2_swp.inc Hys.HYSCORE.T2_swp.inc];
        T2_swp.strt = Hys.HYSCORE.T2_swp.strt + chirper.events{3}.t + [0 chirper.events{2}.t]; 
        T2_swp.dim = Hys.HYSCORE.T2_swp.dim;
        T2_swp.axis = (T2_swp.strt(1) - chirper.events{3}.t) + T2_swp.inc(1) * (0:T2_swp.dim-1).';
        
        % run single echo to set B-field
        chirper.parvars = {pcyc3_4p};
        chirper.savename = '';
        launch(chirper,conf_HYSCORE); % launch overwrites conf in base workspace
        conf_HYSCORE.act = evalin('base','conf.act');
        brk_interface('wait');
        pause(round(1e-9*chirper.shots*chirper.reptime*length(chirper.parvars{1}.vec(:,1))) + 3);

        chirper.parvars = {pcyc3_4p,T2_swp};

        % tau increment
        % T1_vec = 400:4:1200;     %choose same values as for T2 in chirp_4pecho.m!
    %     T1_vec = T2_swp_here.strt(1):T2_swp_here.inc(1):(T2_swp_here.strt(1) + (T2_swp_here.dim-1)*T2_swp_here.inc(1));
        
        
        T1_vec = T1:Hys.HYSCORE.T1_swp.inc:(T1 + (Hys.HYSCORE.T1_swp.dim-1)*Hys.HYSCORE.T1_swp.inc);
        T1strt = T1_vec(1);
        
        for ii = 1:length(T1_vec)
            T1 = T1_vec(ii);

            chirper.events{3}.t = chirper.events{2}.t + T1; % move 3rd pulse
            chirper.parvars{2}.strt =  T1strt  + chirper.events{3}.t + [0 chirper.events{2}.t];    %starting point of last pulse and detection

            chirper.savename = ['4pHYSCORE_' num2str(Hys.temp.value) 'K_tau_' num2str(tau) 'ns_T1_' num2str(round(T1))];

            % HYSCORE (4pESEEM) - current trace
            launch(chirper,conf_HYSCORE); % important: no change in conf anymore compared to above
            
%             brk_interface('wait');
            
            pause(10); % DAKL pause directly after launch is critical. Here should be no B-field change!
           
%             while(~any(foo(:,end)~=0) )
%                 pause(time);
%                 foo = dig_interface('get');
%                 i2Dcut_nox((1:size(foo,1)),1:size(foo,2),foo,2);
%             end
            

            while (dig_interface('savenow'))

                pause(5); % DAKL 1->5

                desktop = com.mathworks.mde.desk.MLDesktop.getInstance;
                cmdWindow = desktop.getClient('Command Window');
                cmdWindowScrollPane = cmdWindow.getComponent(0);
                cmdWindowScrollPaneViewport = cmdWindowScrollPane.getComponent(0);
                cmdTextUI = cmdWindowScrollPaneViewport.getComponent(0);
                cmdText = cmdTextUI.getText;
                one_string = string(cmdText);
                collum_lines = regexp(one_string, '\n', 'split');
                last_string = collum_lines(end-1);

                displayed_data = sscanf(last_string, 'Ongoing acquisition, %i available and stored. %f to go, which will take some %f minutes');
                
                try
                    if strcmp(last_string, "Acquisition is writing -> No data was saved")|| displayed_data(3) == inf
                        pause(1);
                        continue
                    end
                catch
                    pause(20);
                    continue
                end
                time = displayed_data(3) * 60;    %seconds

                if time < 10
                    pause(5);
                elseif time > 600
                    pause(600);
                else
                    pause(time - 5);
                end    
            end
            
            if ii == 2 || ii == length(T1_vec) ||  ~mod(ii,plot_rate)
                try
                    %plot echos
                    pause(1);
                    foo = dig_interface('get');
                    i2Dcut(foo,1);

                    contents = dir([directory '/*4pHYSCORE*.mat']); %

                    % determine file order by T1 from file name
                    for i = 1:(length(contents))
                        str_here = strsplit(contents(i).name,'_');
                        if length(str_here) > 1
                            contents(i).T1 = str2double(str_here{end}(1:end-4));
                        end
                    end
                    do_T1_sorting = 0;
                    tab = struct2table(contents); % removes also first 2 entries
                    if do_T1_sorting
                        ind_dirs = tab{:,5} == 0;
                        tab_sorted = sortrows(tab(ind_dirs,:), 'T1');
                        contents = table2struct(tab_sorted);
                    else
                        contents = table2struct(tab);
                    end

                    clear options;
                    options.evlen = 128; 
                    options.plot = 0;
                    options.find_echo = 0;
                    options.phase_all = 0;
                    options.corr_phase = -1.09; %-0.5; %1.5; %

                    file = contents(1).name;

                    output = uwb_eval2([directory '/' file],options);

                    A = output.dta_ev;

                    phase_corr_values = zeros(length(contents),1);

                    ext_values = zeros(length(contents),1);
                    load([directory '/' file],'ext');
                    ext_values(1) = ext.gap_join(3);
                    unfinished = 0;
                    directories = 0;

                    for j = 2:(length(contents))

                        if contents(j).isdir
                            directories = directories + 1;
                            continue
                        end
                        file = contents(j).name;
                        load([directory '/' file],'ext');
                        ext_values(j) = ext.gap_join(3);

                        try
                            output = uwb_eval2([directory '/' file],options);
                        catch
                            warning(['Could not read file ' file]);
                            unfinished = 1;
                            break;
                        end
                        B = output.dta_ev;
                        A = [A;B];
                        phase_corr_values(j) = output.corr_phase;

                    end

                    figure(501); clf;

                    s = surf(output.dta_x{1}, 1:(length(contents)-unfinished-directories), real(A));

                    s.EdgeColor = 'none';
                    hold on;
                catch
                    warning('Error with plotting, measurement is continued')
                end
            end
            
            
            
        end
    end
end   
    is_finished = 1;
end