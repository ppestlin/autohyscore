function [final_reptime, final_shots, signal_to_noise,signal_to_noise_per_time, conf_4pESEEM,directory] = ESEEM_4p(Hys, pulse_fields)
    
    clear chirper;
    
    % Might want to set wd again here, just to make sure
    
    directory = [Hys.save_directory.ESEEM_4p '_B_' num2str(round(Hys.b_field.value)) 'G'];
    mkdir(directory);
    
    
    tau = Hys.ESEEM4p.tau;
    T1 = Hys.ESEEM4p.T1;
    
    clear snr_per_time;
    bsl_corrected = 'max intensity';
    
    [chirper] = get_4pESEEM(Hys, pulse_fields, tau, T1);
    
    current_reptime = round(Hys.myreptime.value,-3);
    final_reptime = current_reptime;
    chirper.savepath = ['./' directory];                     
    
    clear pcyc3_4p;
    pcyc3_4p.name = '8-step echo cycle for HYSCORE';
    pcyc3_4p.variables = {'events{1}.pulsedef.phase','events{4}.pulsedef.phase','events{3}.pulsedef.phase','events{5}.det_sign'};
    pcyc3_4p.vec = [0    0       0    +1; ...
                    0   pi      0    -1; ...
                    0   0       pi    +1; ...
                    0   pi      pi    -1; ...
                    pi    0       0    -1; ...
                    pi   pi      0    +1; ...
                    pi   0       pi    -1; ...
                    pi   pi      pi    +1; ...
                     ];
    pcyc3_4p.reduce = 1;
    
    %!! Anpassen?
    
    clear T2_swp;
    T2_swp.name = 'T2 incr';
    T2_swp.variables = {'events{4}.t', 'events{5}.t'}; 
    T2_swp.inc = [Hys.ESEEM4p.T2_swp.inc Hys.ESEEM4p.T2_swp.inc]; % over night HYSCORE
    T2_swp.strt = Hys.ESEEM4p.T2_swp.strt + chirper.events{3}.t + [0 chirper.events{2}.t];  %center nuclear coherence echo: T1=800 in middle of T2_swp
    T2_swp.dim = Hys.ESEEM4p.T_2swp_dim; %200; %801; %1024; %256;
    T2_swp.axis = (T2_swp.strt(1) - chirper.events{3}.t) + T2_swp.inc(1) * (0:T2_swp.dim-1).';
    
    T1_vec = Hys.HYSCORE.T1_swp.strt:Hys.HYSCORE.T1_swp.inc:(Hys.HYSCORE.T1_swp.strt + (Hys.HYSCORE.T1_swp.dim-1)*Hys.HYSCORE.T1_swp.inc);
    
    Hys.myreptime.min = round((max(Hys.taus) +  T2_swp.axis(end) + T1_vec(end) + 1.5e3) * 1.1/Hys.conf_start.std.dutyX, -3);
    min_reptime = Hys.myreptime.min;
    max_reptime = Hys.myreptime.value;
    if max_reptime < min_reptime
        max_reptime = min_reptime;
    end
    step_reptime = Hys.ESEEM4p.reptime_stepsize;
    reptimes = [max_reptime,- step_reptime,min_reptime];
    if ~ismember(min_reptime,reptimes)
        reptimes = [reptimes,min_reptimes];
    end
    
    snr_per_time = zeros(length(reptimes));
    snr = snr_per_time;
    
    points = T2_swp.dim;
    
    chirper.parvars = {pcyc3_4p};
    chirper.savename = '';

    conf_4pESEEM = check_ringing(Hys.conf_start,Hys.resonator,chirper);
    
    if ~isstruct(conf_4pESEEM)
        save('Hyscore_structure_before_error.mat','Hys')
        error('Ringing Check for 4pESEEM did not pass')
    end
    chirper.parvars = {pcyc3_4p,T2_swp};

    sufficient = 1;
    i = 1;

    while sufficient
        
        pause(2);
        
        chirper.reptime = current_reptime;
        chirper.savename = ['4pESEEM_tau_' num2str(tau) 'ns_T1_' num2str(T1) '_' num2str(round(current_reptime/1e5)) 'e-1ms'];
        launch(chirper,conf_4pESEEM); % launch overwrites conf in base workspace
        conf_4pESEEM.act = evalin('base','conf.act');
        
        brk_interface('wait');
%         pause(5); % DAKL pause directly after launch is critical
        
        while (dig_interface('savenow'))
        
            pause(5); % DAKL: 1->5

            desktop = com.mathworks.mde.desk.MLDesktop.getInstance;
            cmdWindow = desktop.getClient('Command Window');
            cmdWindowScrollPane = cmdWindow.getComponent(0);
            cmdWindowScrollPaneViewport = cmdWindowScrollPane.getComponent(0);
            cmdTextUI = cmdWindowScrollPaneViewport.getComponent(0);
            cmdText = cmdTextUI.getText;
            one_string = string(cmdText);
            collum_lines = regexp(one_string, '\n', 'split');
            last_string = collum_lines(end-1);

            displayed_data = sscanf(last_string, 'Ongoing acquisition, %i available and stored. %f to go, which will take some %f minutes');

            try
                if strcmp(last_string, "Acquisition is writing -> No data was saved")|| displayed_data(3) == inf
                    pause(1);
                    continue
                end
            catch
                pause(20);
                continue
            end

            time = displayed_data(3) * 60;    %seconds

            if time < 10
                pause(5);
            elseif time > 600
                pause(600);
            else
                pause(time - 5);
            end    
        end

        % options for uwb_eval
        clear options;
        options.plot = 0; % do not plot by uwb_eval
%         options.evlen = 256; %128; %56; % enforce a 256 point evaluation window
        options.phase_all = 0; %0 for nutation, 1 for ampsweeps;

        % get data
        file = evalin('base', 'currfilename');
        output = uwb_eval2([chirper.savepath '/' file],options);
        dta_ev = output.dta_ev(1:end); % echo integrals
        ev_coll = dta_ev;
        dta_x_cont = output.dta_x{1}(1:end); % axis of first indirect dimension
    
        
        % Plot raw data
        
%         figure(i); clf; hold on;
%         plot(dta_x_cont{1}(1:end),real(ev_coll(1:end)),'-')
%         ylabel('Re( Echo integral ) [arb]')
%         xlabel('Sweep axis')
        
        myreptime = current_reptime;
        shots = chirper.shots;
        measure_time = shots*points*8*myreptime/(1e9*60); %min
        % renaming variables

        t=(dta_x_cont);
        vexp=transpose(real(ev_coll));

        % baseline fitting

        BGstart = 1;
        BGtype = 'poly';

        switch BGtype
            case 'poly'
                [p,~]=polyfit(t(BGstart:end),vexp(BGstart:end),1); % fit the baseline by a polynomial
                bsl=polyval(p,t); % compute the optimum baseline
            case 'sexp'
                tbg = t(BGstart:end)';
                vexpf = vexp(BGstart:end)';
                fun = @(vec,tbg,vexpf) sum(sqrt((vexpf - (vec(1).*exp(-(tbg./vec(2)).^vec(3))+vec(4))).^2)); % RMSD(data-baseline)
                fun2 = @(vec)fun(vec,tbg,vexpf);
                options = optimset('MaxFunEvals',1e8,'MaxIter',1e6);
                vec = fminsearch(fun2,[vexpf(1) 100 1 vexpf(end)],options);
                bsl = vec(1).*exp(-(t./vec(2)).^vec(3))+vec(4); % compute the optimum baseline
            case 'bisexp'
                tbg = t(BGstart:end)';
                vexpf = vexp(BGstart:end)';
                fun = @(vec,tbg,vexpf) sum(sqrt((vexpf - (vec(1).*exp(-(tbg./vec(2)).^vec(3)) + vec(4).*exp(-(tbg./vec(5)).^vec(6)) + vec(7))).^2)); % RMSD(data-baseline)
                fun2 = @(vec)fun(vec,tbg,vexpf);
                options = optimset('MaxFunEvals',1e8,'MaxIter',1e6);
                vec = fminsearch(fun2,[vexpf(1) 10 1 vexpf(1)./2 100 1 vexpf(end)],options);
                bsl = vec(1).*exp(-(t./vec(2)).^vec(3)) + vec(4).*exp(-(t./vec(5)).^vec(6)) + vec(7); % compute the optimum baseline
            otherwise
                % 4pESEEM use 'poly' as standard to make sure the code does not stop
                [p,~]=polyfit(t(BGstart:end),vexp(BGstart:end),2); % fit the baseline by a high-order polynomial
                bsl=polyval(p,t); % compute the optimum baseline
        end
        figure('Name',['4pESEEM with bsl and corrected' num2str(current_reptime/1e5) 'e-1ms']); clf; % in Figure 2, the baseline correction is shown
        plot(t,vexp,'k'); % original data are plotted BLACK
        hold on;
        plot(t,bsl,'r'); % fitted baseline (decay function) is plotted RED
        plot(t,vexp-bsl,'b'); % baseline-corrected data are plotted BLUE
       

        switch bsl_corrected
            case 'mod depth'
                [vexp_up, vexp_low] = envelope(vexp-bsl,10, 'peak');
%                 figure(10 + i); clf; hold on;
%                 plot(t,vexp,'g')
%                 plot(t,bsl,'r')
%                 plot(t,vexp_low,'b')
%                 plot(t,vexp_up,'b')
                signal = vexp_up(1)-vexp_low(1);
                noise = std(vexp(round(0.9*length(vexp)):end)-bsl(round(0.9*length(vexp)):end));
                snr_per_time(i) = signal/(noise*measure_time);

            case 'max intensity'
%                 figure(10 + i); clf; hold on;
%                 plot(t,vexp)
                noise = std(vexp(round(0.9*length(vexp)):end));
                if i == 1
                    [~,max_ind] = max(vexp);
                    max_up = max_ind + 2;
                    max_low = max_ind - 2;
                    if max_low < 1
                        max_low = 1;
                    end
                end
                signal = mean(vexp(max_low:max_up));
                snr(i) = signal/noise;
                snr_per_time(i) = signal/(noise*measure_time);
            otherwise
%                 figure(10 + i); clf; hold on;
%                 plot(t,vexp)
                noise = std(vexp(round(0.9*length(vexp)):end));
                if i == 1
                    [~,max_ind] = max(vexp);
                    max_up = max_ind + 2;
                    max_low = max_ind - 2;
                    if max_low < 1
                        max_low = 1;
                    end
                end
                signal = mean(vexp(max_low:max_up));
                snr(i) = signal/noise;
                snr_per_time(i) = signal/(noise*measure_time);

        end
       
        if i > 1
            if snr_per_time(i) < snr_per_time(1)* 0.9|| snr_per_time(i) < snr_per_time(i-1)*0.9
                sufficient = 0;
                signal_to_noise = snr(i-1);
                signal_to_noise_per_time = snr_per_time(i-1);
            elseif current_reptime == min_reptime
                final_reptime = current_reptime;
                signal_to_noise = snr(i);
                signal_to_noise_per_time = snr_per_time(i-1);
                sufficient = 0;
            else
                i = i + 1;
                final_reptime = current_reptime;
                current_reptime = current_reptime - step_reptime;
                if current_reptime < min_reptime
                    current_reptime = min_reptime;
                end
            end
        else
            i = i + 1;
            final_reptime = current_reptime;
            current_reptime = current_reptime - step_reptime;
            if current_reptime < min_reptime
                current_reptime = min_reptime;
            end
        end
    end
    
    shots_factor = Hys.HYSCORE.snr / signal_to_noise;
    final_shots = chirper.shots * (shots_factor)^2;
    if final_shots < Hys.HYSCORE.min_shots
        final_shots = Hys.HYSCORE.min_shots;
    end

end