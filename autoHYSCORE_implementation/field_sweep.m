function [gyro_exp,clipping,Hyscore_fields,file,changed_fields] = field_sweep(Hys, pulse)
clear echo;

[echo, changed_fields] = get_hahn_echo(Hys, pulse, 'field sweep');

% Defintion of field sweep
clear Bswp;
sweepwidth = 1000; % G 
Bswp.name = 'Bswp';
Bswp.variables = 'B';
Bswp.dim = 1001;
Bswp.strt = echo.B - sweepwidth/2;
Bswp.inc = sweepwidth / (Bswp.dim - 1);


% Definition of phase cycling
clear pcyc;
pcyc.name = 'o1.2';
pcyc.variables = {'events{1}.pulsedef.phase','events{3}.det_sign'};
pcyc.ax_id = 0;
pcyc.vec = [0 1; pi -1];
pcyc.reduce = 1;

% Set parameters of echo and savename for the experiment
echo.parvars = {pcyc,Bswp};

echo.savename = ['2pecho_field_sweep'];

echo.VideoGain = Hys.VideoGain.value - 5;
if echo.VideoGain < -31
    echo.VideoGain = -31;
end

% run the experiment
launch(echo);

pause(5);

while (dig_interface('savenow'))

    pause(5);

    desktop = com.mathworks.mde.desk.MLDesktop.getInstance;
    cmdWindow = desktop.getClient('Command Window');
    cmdWindowScrollPane = cmdWindow.getComponent(0);
    cmdWindowScrollPaneViewport = cmdWindowScrollPane.getComponent(0);
    cmdTextUI = cmdWindowScrollPaneViewport.getComponent(0);
    cmdText = cmdTextUI.getText;
    one_string = string(cmdText);
    collum_lines = regexp(one_string, '\n', 'split');
    last_string = collum_lines(end-1);

    displayed_data = sscanf(last_string, 'Ongoing acquisition, %i available and stored. %f to go, which will take some %f minutes');

    try
        if strcmp(last_string, "Acquisition is writing -> No data was saved")|| displayed_data(3) == inf
            pause(1);
            continue
        end
    catch
        pause(10);
        continue
    end

    time = displayed_data(3) * 60;    %seconds

    if time < 10
        pause(5);
    elseif time > 600
        pause(600);
    else
        pause(time - 5);
    end

end

file = evalin('base', 'currfilename');

clear options;
% options.evlen = 1000; %Length of the window to store the echo transient and perform the integration. By default, UWB_EVAL searches the best range
options.plot = 0; % no plots generated
options.phase_all = 0; % phase all data points, since amplitude sweeps make no sense without phasing

% output = uwb_eval(file,options);
output = uwb_eval2(file,options);

dta_x = output.dta_x{1}; % Cell of indirect dimensions of the experiment here B from B sweep
dta_ev = output.dta_ev; % Integrated echo transients
LO_freq = output.exp.LO; % local oscillator frequency
det_frq = output.det_frq; % intermediate frequency

[peaks_found, peaks_ind] = findpeaks(real(dta_ev)/max(real(dta_ev)),dta_x,...
    'MinPeakProminence',0.001, 'MinPeakHeight',0.1);

peaks_found = peaks_found * max(real(dta_ev));

[~,max_ind_peaks_found] = max(peaks_found);

ind_vec = 1:length(peaks_found);
ind_vec = ind_vec - max_ind_peaks_found;

Hyscore_fields = [];
chosen_peak_hights = [];
for i = 1:length(ind_vec)
    if ismember(ind_vec(i),Hys.b_field.peaks)
        Hyscore_fields = [Hyscore_fields,round(peaks_ind(i),2)];
        chosen_peak_hights = [chosen_peak_hights,peaks_found(i)];
    end
end

if isempty(Hyscore_fields)                          % If we dont find a matching peak, measure maximum. Change to measure next lowest/ highest?
                                                    % Can get very complicated
    [~,maid] = max(real(dta_ev));
    B_obs = dta_x(maid); % maximum of spectrum
    Hyscore_fields = round(B_obs,2);
else
    [~,maid] = max(chosen_peak_hights);
    B_obs = Hyscore_fields(maid);
end



% Part from Uwbeval, gives best video gain and chenges accordingly
vid_range = -31:1:20; 
dig_level_0dB = output.dig_level*10^(-1*echo.VideoGain/20);
possible_levels = dig_level_0dB .* 10.^(vid_range./20); %conf.std.IFgain_levels / conf.std.IFgain_levels(estr.VideoGain+1);

    % crop the too large levels
possible_levels(possible_levels > 0.75) = 0;
[~,best_idx] = max(possible_levels);
vid_diff = vid_range(best_idx) - echo.VideoGain;
if vid_diff < 0 || vid_diff > 10
    clipping = 1;
    gyro_exp = (LO_freq+det_frq)/B_obs;
    warning(['The field sweep detected clipping at ' ... 
                 'gyro_exp is now set to ' num2str(gyrp_exp) ', which might not be perfect' ...
                 'but the run will be continued']);
    changed_fields.VideoGain.value =  vid_range(best_idx);
else
    clipping = 0;
    gyro_exp = (LO_freq+det_frq)/B_obs; % experimental gyromagnetic ratio
    % this may be used for setting B in other experiments, or just to calculate
    % the field corresponding to B_obs at a different microwave frequency;
end


% plot the field-domain spectra
figure('Name','Last field sweep'); clf; hold on; 
hold on; 
plot(dta_x,real(dta_ev)); %plot(dta_x,dta_dc(128,:),'r')
xlabel('B_0 [G]')
ylabel('echo intg')
title('Exp. Fieldswp')
axis tight;

disp('This script updated the variable "gyro_exp" for B0 calculations');
end

