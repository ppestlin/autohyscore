function [taus, conf_3pESEEM,directory] = ESEEM_3p(Hys, pulse_fields)


% Might want to set wd again here, just to make sure

directory = [Hys.save_directory.ESEEM_3p '_B_' num2str(round(Hys.b_field.value)) 'G'];
mkdir(directory);

tau_vec = Hys.ESEEM3p.tau_start:Hys.ESEEM3p.tau_step:Hys.ESEEM3p.tau_end;
T = 128;
chirper = get_3pESEEM(Hys,pulse_fields,tau_vec(1),128);

clear T_swp;
T_swp.name = 'T incr';
T_swp.variables = {'events{3}.t', 'events{4}.t'};
T_swp.inc = 1.1111111111*[12 12];
T_swp.strt = T * [1 1] + chirper.events{2}.t*[1 2];
T_swp.ax_id = 2;
T_swp.dim = 512;

% specification of phase cycle with last pulse also cycled
clear pcyc2;  %add up: '+ - - +'
pcyc2.name = '4-step echo cycle';
pcyc2.variables = {'events{1}.pulsedef.phase','events{2}.pulsedef.phase','events{4}.det_sign'};
pcyc2.vec = [   0       0      +1; ...
                pi      0      -1; ...
                0       pi     -1; ...
                pi      pi     +1; ...
                 ];
pcyc2.reduce = 1;

chirper.parvars = {pcyc2};
chirper.savename = '';

swp_end = (T_swp.strt(2) + T_swp.inc(2) * (T_swp.dim - 1));
    
    
if Hys.myreptime.value < (swp_end/Hys.conf_start.std.dutyX)
    Hys.myreptime.value = round(1.1*swp_end/Hys.conf_start.std.dutyX,-3);
end

conf_3pESEEM = check_ringing(Hys.conf_start,Hys.resonator,chirper);

if ~isstruct(conf_3pESEEM)
    save('Hyscore_structure_before_error.mat','Hys')
    error('Ringing Check for 3pESEEM did not pass')
end



for i = 1:length(tau_vec)
    
    clear chirper;
    tau = tau_vec(i);
    T = 128;
    
    chirper = get_3pESEEM(Hys,pulse_fields,tau,T);
    
    chirper.parvars = {pcyc2, T_swp};
    chirper.savepath = ['./' directory];                     
    chirper.savename = ['3pESEEM_tau_' num2str(tau) 'ns'];
    
    launch(chirper,conf_3pESEEM);
    
    pause(5); % DAKL pause directly after launch is critical. Here should be no B-field change!
           
    while (dig_interface('savenow'))
        
        pause(5); % DAKL: 1->3

        desktop = com.mathworks.mde.desk.MLDesktop.getInstance;
        cmdWindow = desktop.getClient('Command Window');
        cmdWindowScrollPane = cmdWindow.getComponent(0);
        cmdWindowScrollPaneViewport = cmdWindowScrollPane.getComponent(0);
        cmdTextUI = cmdWindowScrollPaneViewport.getComponent(0);
        cmdText = cmdTextUI.getText;
        one_string = string(cmdText);
        collum_lines = regexp(one_string, '\n', 'split');
        last_string = collum_lines(end-1);
    
        displayed_data = sscanf(last_string, 'Ongoing acquisition, %i available and stored. %f to go, which will take some %f minutes');
        
        try
            if strcmp(last_string, "Acquisition is writing -> No data was saved")|| displayed_data(3) == inf
                pause(1);
                continue
            end
        catch
            pause(20);
            continue
        end
            
        time = displayed_data(3) * 60;    %seconds
    
        if time < 10
            pause(5);
        elseif time > 600
            pause(600);
        else
            pause(time - 5);
        end    
    end
    
end

clear contents;

n = Hys.ESEEM3p.trace_n;

change_sens = 0.001;    % Threshhold in change point (gives linear slopes)
slope_sens = 1;         % Slope when 'not in peak area' anymore
peak_sens = 0.025;      % The larger -> the larger a peak needs to bo to be
                        % detected
peak_dist = 0.001;       % Min distance between 2 peeks

ev_case = 'peak spots'; % 'first to last intervall boundary', evaluation cases

% Disclaimer: These variables are tuned to a normalized y-Achses. They
% might need a lot more tuning, but, if you dont want to analyze nothing or
% analyze the entire spectrum, should be handled with care

% Get file names
contents = dir(chirper.savepath);

for i = 1:(length(contents))
    str_here = strsplit(contents(i).name,'_');
    if length(str_here) > 1
        contents(i).tau = str2double(str_here{end}(1:end-6));
        
        % It might be advantagous to change the save name to
        % ...time_ns.mat or even ns_time.mat to keep order with HYSCORE dta
        
    end
end

% Sort data
tab = struct2table(contents(3:end)); % removes also first 2 entries
tab_sorted = sortrows(tab, 'tau');
contents = table2struct(tab_sorted);
contents_len = length(contents);

% options for uwb_eval
clear options;
options.plot = 0; % do not plot by uwb_eval
% options.evlen = 256; %128; %56; % enforce a 256 point evaluation window
options.phase_all = 0; %0 for nutation, 1 for ampsweeps;

% Initialize variables and figures
dta_cont = cell(contents_len,1);
dta_x_cont = dta_cont;
ev_coll = dta_cont;
exp_cont = dta_cont;

% Get raw data

for i=1:contents_len
    file = contents(i).name;
    output = uwb_eval2([chirper.savepath '/' file],options);
    dta_cont{i} = output.dta_avg; % averaged echo transients
    dta_ev = output.dta_ev(1:end); % echo integrals
    ev_coll{i} = dta_ev;
    dta_x_cont{i} = output.dta_x{1}(1:end); % axis of first indirect dimension
    exp_cont{i} = output.exp;
end

% Delete traces with a maximum intensity of less than 1/3 of the maximum
% !Not finished/ weird problems!


% Plot raw data

cmap =hsv(length(contents)+1)*0.7; % For the colors in the plot :)
figure('Name','3pESEEM raw data'); clf; hold on;
for i = 1:length(contents)
    plot(dta_x_cont{i}(1:end),real(ev_coll{i}(1:end)),'-',...
    'Color',cmap(i,:))
end
ylabel('Re( Echo integral ) [arb]')
xlabel('Sweep axis')


%% Plot traces and sum


% Needs change in final code, just for testing
% !!

abs_sum = 0;
clear changes;              % Changes only needed for plotting of single FT
spc = cell(contents_len,1);

for i = 1:contents_len
    % renaming variables
    
    t=(dta_x_cont{i});
    vexp=transpose(real(ev_coll{i}));
    
    % baseline fitting
    
    BGstart = 1;
    BGtype = 'poly';
    
    switch BGtype
        case 'poly'
            [p,s]=polyfit(t(BGstart:end),vexp(BGstart:end),2); % fit the baseline by a high-order polynomial
            bsl=polyval(p,t); % compute the optimum baseline
        case 'sexp'
            tbg = t(BGstart:end)';
            vexpf = vexp(BGstart:end)';
            fun = @(vec,tbg,vexpf) sum(sqrt((vexpf - (vec(1).*exp(-(tbg./vec(2)).^vec(3))+vec(4))).^2)); % RMSD(data-baseline)
            fun2 = @(vec)fun(vec,tbg,vexpf);
            options = optimset('MaxFunEvals',1e8,'MaxIter',1e6);
            vec = fminsearch(fun2,[vexpf(1) 100 1 vexpf(end)],options);
            bsl = vec(1).*exp(-(t./vec(2)).^vec(3))+vec(4); % compute the optimum baseline
        case 'bisexp'
            tbg = t(BGstart:end)';
            vexpf = vexp(BGstart:end)';
            fun = @(vec,tbg,vexpf) sum(sqrt((vexpf - (vec(1).*exp(-(tbg./vec(2)).^vec(3)) + vec(4).*exp(-(tbg./vec(5)).^vec(6)) + vec(7))).^2)); % RMSD(data-baseline)
            fun2 = @(vec)fun(vec,tbg,vexpf);
            options = optimset('MaxFunEvals',1e8,'MaxIter',1e6);
            vec = fminsearch(fun2,[vexpf(1) 10 1 vexpf(1)./2 100 1 vexpf(end)],options);
            bsl = vec(1).*exp(-(t./vec(2)).^vec(3)) + vec(4).*exp(-(t./vec(5)).^vec(6)) + vec(7); % compute the optimum baseline
        otherwise
            % use 'sexp' as standard to make sure the code doesn't stop
            tbg = t(BGstart:end)';
            vexpf = vexp(BGstart:end)';
            fun = @(vec,tbg,vexpf) sum(sqrt((vexpf - (vec(1).*exp(-(tbg./vec(2)).^vec(3))+vec(4))).^2)); % RMSD(data-baseline)
            fun2 = @(vec)fun(vec,tbg,vexpf);
            options = optimset('MaxFunEvals',1e8,'MaxIter',1e6);
            vec = fminsearch(fun2,[vexpf(1) 100 1 vexpf(end)],options);
            bsl = vec(1).*exp(-(t./vec(2)).^vec(3))+vec(4); % compute the optimum baseline
    end
%     figure(2); clf % in Figure 2, the baseline correction is shown
%     plot(t,vexp,'k'); % original data are plotted BLACK
%     hold on;
%     plot(t,bsl,'r'); % fitted baseline (decay function) is plotted RED
%     plot(t,vexp-bsl,'b'); % baseline-corrected data are plotted BLUE
%     plot([t(BGstart) t(BGstart)],[min((vexp-bsl)) max(vexp)],'k:'); % dotted line denotes start of background fit
    modul=transpose((vexp./bsl-ones(size(bsl)))); % deconvolute baseline (decay function) and subtract constant baseline
    
%     figure(31); % Figure 3 shows preprocessing of the nuclear modulation
%     plot(t,modul,'Color',cmap(i,:)); % raw modulation data are plotted BLACK
%     hold on;
    modul1=hamming_gj(modul,1.5); % apply hamming apodization function
    modul2=[modul1(1:length(modul1)) zeros(1,3*length(modul1))]; % left shift data by ls points and zero fill to four times the length
    % plot(tf,modul2,'r'); % apodized and zerofilled data are plotted RED
    
    
    % Fourier transformation
    
    spc{i}=fftesr(modul2); % compute Fourier transform of zero-filled and apodized data
    dt=t(2)-t(1); % time increment
    ny=linspace(0,1/(2*dt),round(length(spc{i})/2)); % frequency axis (positive frequencies only)
    spc{i}=abs(spc{i}(1:round(length(spc{i})/2))); % cut the spectrum, show only positive frequencies
    
    

    % Single trace plotting, Change with Code from below
    % !!
    
%     changes = ischange(abs(spc{i}),'Threshold',0.1);
%     figure(i); clf;
%     plot(ny,abs(spc{i}),'Color','blue');
%     hold on;
%     for ii = 1:length(changes)
%         if changes(ii) == 1
%             plot(ny(ii), abs(spc{i}(ii)), '.', 'Color', 'red')
%         end
%     end
%     set(gca,'FontSize',20);
%     xlabel('\nu (MHz)');

        
    
    
    abs_sum = abs_sum + abs(spc{i});
    
end



% Normalization constants

abs_sum = abs_sum/contents_len;
abs_sum_max4norm = max(abs_sum);
% abs_sum = abs_sum/max(abs_sum);

%% Find changes new

ny_start = 1;

% while ny(ny_start) < 0.004
%     ny_start = ny_start + 1;
% end

abs_sum_cutoff = abs_sum(ny_start:end)/abs_sum_max4norm;
ny_cutoff = ny(ny_start:end);

% Find change points
[changeIndices, segmentSlope, segmentIntercept] = ischange(...
    abs_sum/abs_sum_max4norm, "linear","Threshold",change_sens,...
    "SamplePoints",ny);

% Display results

% figure(50);clf
% plot(ny, abs_sum/abs_sum_max4norm,"Color",[77 190 238]/255,"DisplayName",...
%     "Input data")
% hold on
% 
% % Plot segments between change points
% plot(ny,segmentSlope(:).*ny(:)+segmentIntercept(:),"Color",[64 64 64]/255,...
%     "DisplayName","Linear regime")
% 
% % Plot change points
% 
% for i = 1:length(changeIndices)
%     if changeIndices(i) == 1
%         plot(ny(i), segmentSlope(i).*ny(i)+segmentIntercept(i), '.',...
%             'Color', 'red')
%     end
% end
% 
% title("Number of change points: " + nnz(changeIndices));
% hold off;
% xlabel("ny");

%% Characterize traces and sum

% corrcoef(abs(spc{1}),abs(spc{4}));

[peaks_found, peaks_ind] = findpeaks(abs_sum/abs_sum_max4norm,ny,...
    'MinPeakProminence',peak_sens,'MinPeakDistance',peak_dist);

% !! Final Peak Prominence vermutlich
% ??ndern. So kriegt man jetzt den kleinen
% peak aber man L??uft auch gefahr
% ungewollte features mit zu betrachten

% Plot for peaks

% figure(51);clf;
% 
% findpeaks(abs_sum(ny_start:end)/abs_sum_max4norm,ny(ny_start:end),...
%     'MinPeakProminence',peak_sens, 'MinPeakDistance', peak_dist, 'Annotate', 'extents')

%%

corr_ind = [];
corr_coef_list = cell(contents_len,1);
peaks_used = [];

if isempty(peaks_found)             % To make sure if the tuning was wrong we still compare the entire spectra
    peaks_ind = [ny(1), ny(end)];
end
    
    for i = 1:length(peaks_found)
        
        
        ii = find(ny==peaks_ind(i));    % Goes to the left
        jj = ii + 5;                        % Goes to the right
        ii = ii - 5;
        if ii < 1
            ii = 1;
        end
        if jj > length(segmentSlope)
            jj = length(segmentSlope);
        end
        while segmentSlope(ii) > slope_sens && ii > 1
            ii = ii - 1;
        end
        while segmentSlope(jj) < -slope_sens && jj < length(segmentSlope)
            jj = jj + 1;
        end
        
        ii = round(ii - 10);   % !!Might change in final
        jj = round(jj + 10);
        
        if ii < 1
            ii = 1;
        end 
        if jj > length(segmentSlope)
            jj = length(segmentSlope);
        end
%   Changed 1.1   
%   New format now finds the largest peak within an intervall.

%         if ismember(jj, corr_ind) || ismember(ii, corr_ind)      
%               continue
        if ismember(jj, corr_ind)
            find_ind = find(jj==corr_ind);
            peaks_used(find_ind/2) = max(peaks_found(i),peaks_used(find_ind/2));

        elseif ismember(ii, corr_ind)
            find_ind = find(ii==corr_ind);
            peaks_used((find_ind+1)/2) = max(peaks_found(i),peaks_used((find_ind+1)/2));

        else
            corr_ind = [corr_ind, ii, jj];              
            peaks_used = [peaks_used, peaks_found(i)];  

        end
        
    end
    
    corr_abs_sum = cell(1,length(corr_ind)/2);
    corr_ny = corr_abs_sum;
    corr_spc = cell(contents_len,length(corr_ind)/2);
    corr_result_spc = zeros(1,length(abs_sum));
    corr_result_ind = zeros(n,1);
    corr_result_tau = zeros(n,1);
   
    for k = 1:n

%       Changed 1.1 
%       Now also features an option 'first to last', evaluating the entire
%       are between the first and last intervall boundary. This case is
%       currently hard set this script and can not yet be changed by the
%       user. To do so introduce field check for ev_case
        switch ev_case
            case 'peak spots'
                for i = 1:2:length(corr_ind)
                    j = i + 1;
                    corr_abs_sum{j/2} = abs_sum(corr_ind(i):corr_ind(j))/...
                        abs_sum_max4norm;
                    corr_ny{j/2} = ny(corr_ind(i):corr_ind(j));
                    for ii = 1:contents_len
                        if ismember(ii,corr_result_ind)
                            continue
                        else
                            corr_spc{ii,j/2} = (corr_result_spc(...
                                corr_ind(i):corr_ind(j)) + spc{ii}(corr_ind(i):corr_ind(j)))...
                                /abs_sum_max4norm;
                            corr_coef = corrcoef(corr_abs_sum{j/2}, corr_spc{ii,j/2});
                            corr_coef_list{ii} = [corr_coef_list{ii}, abs(corr_coef(2,1))];
                            % Plotting
        %                     if i == 2
        %                         if ii < 7
        %                             figure(j/2+ii*100); clf;
        %                             plot(corr_ny{j/2},corr_abs_sum{j/2},'Color','blue');
        %                             hold on;
        %                             plot(corr_ny{j/2},corr_spc{ii,j/2},'Color','red');
        %                         end
        %                     end
                            % End Plotting
                        end
                    end
                end
            case 'first to last'
                i = 1;
                j = length(corr_ind);
                corr_abs_sum{j/2} = abs_sum(corr_ind(i):corr_ind(j))/...
                    abs_sum_max4norm;
                corr_ny{j/2} = ny(corr_ind(i):corr_ind(j));
                for ii = 1:contents_len
                    if ismember(ii,corr_result_ind)
                        continue
                    else
                        corr_spc{ii,j/2} = (corr_result_spc(...
                            corr_ind(i):corr_ind(j)) + spc{ii}(corr_ind(i):corr_ind(j)))...
                            /abs_sum_max4norm;
                        corr_coef = corrcoef(corr_abs_sum{j/2}, corr_spc{ii,j/2});
                        corr_coef_list{ii} = [corr_coef_list{ii}, abs(corr_coef(2,1))];
                        % Plotting
                        if i == 2
                            if ii < 7
                                figure(j/2+ii*100); clf;
                                plot(corr_ny{j/2},corr_abs_sum{j/2},'Color','blue');
                                hold on;
                                plot(corr_ny{j/2},corr_spc{ii,j/2},'Color','red');
                            end
                        end
                        % End Plotting
                    end
                end
            otherwise
                for i = 1:2:length(corr_ind)
                    j = i + 1;
                    corr_abs_sum{j/2} = abs_sum(corr_ind(i):corr_ind(j))/...
                        abs_sum_max4norm;
                    corr_ny{j/2} = ny(corr_ind(i):corr_ind(j));
                    for ii = 1:contents_len
                        if ismember(ii,corr_result_ind)
                            continue
                        else
                            corr_spc{ii,j/2} = (corr_result_spc(...
                                corr_ind(i):corr_ind(j)) + spc{ii}(corr_ind(i):corr_ind(j)))...
                                /abs_sum_max4norm;
                            corr_coef = corrcoef(corr_abs_sum{j/2}, corr_spc{ii,j/2});
                            corr_coef_list{ii} = [corr_coef_list{ii}, abs(corr_coef(2,1))];
                            % Plotting
        %                     if i == 2
        %                         if ii < 7
        %                             figure(j/2+ii*100); clf;
        %                             plot(corr_ny{j/2},corr_abs_sum{j/2},'Color','blue');
        %                             hold on;
        %                             plot(corr_ny{j/2},corr_spc{ii,j/2},'Color','red');
        %                         end
        %                     end
                            % End Plotting
                        end
                    end
                end
        end

        peaks_sum = sum(peaks_used);
        corr_coef_final = zeros(1,contents_len);
        for i = 1:length(peaks_used)
            for ii = 1:contents_len
                if ismember(ii,corr_result_ind)
                    corr_coef_final(ii) = 0;
                else
                    corr_coef_final(ii) = corr_coef_final(ii) + (peaks_used(i)...
                        /peaks_sum) * corr_coef_list{ii}(i);
                end
            end
        end
        
        [max_corr, max_corr_ind] = max(corr_coef_final);
        corr_result_spc = plus(corr_result_spc, spc{max_corr_ind});
        corr_result_ind(k) = max_corr_ind;
        corr_result_tau(k) = contents(corr_result_ind(k)).tau;
    end
    figure('Name','3pESEEM final result'); clf; hold on;
    plot(ny, corr_result_spc/n, 'Color', 'red')
    plot(ny, abs_sum,'Color','blue')


hold off;

taus = corr_result_tau;

end