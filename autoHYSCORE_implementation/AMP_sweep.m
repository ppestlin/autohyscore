% AMP sweep function

% READY



function [scale, VideoGain, file, changed_fields] = AMP_sweep(Hys, pulse, clear_files)

% Clear files:  0: Keep all recorded files
%               1: Keep last recorded and delete rest
%               2: Delete all

    clear amp;
    amp.name = 'ampswp both';
    amp.variables = {'events{1}.pulsedef.scale','events{2}.pulsedef.scale'};
    amp.inc = [0.005 0.005];
    amp.strt = [0.0 0.0];
    amp.dim = 100;

    % Definition of phase cycling
    clear pcyc;
    pcyc.name = 'o1.2';
    pcyc.variables = {'events{1}.pulsedef.phase','events{3}.det_sign'};
    pcyc.ax_id = 0;
    pcyc.vec = [0 1; pi -1];
    pcyc.reduce = 1;
    
    pulse_str = num2cell(string(pulse));
    % init figures
    fig_amp = figure('Name',convertStringsToChars(strcat('2pEcho ampswp ', pulse_str{1}, '_', pulse_str{2})));
    

if Hys.IR.executed == 0
    clear echo;

    scale = 1;

    [echo, ~] = get_hahn_echo(Hys, pulse,'AMP sweep');

    % Set parameters of echo and savename for the experiment
    echo.parvars = {pcyc,amp};

    pulse_str = num2cell(string(pulse));

    echo.savename = convertStringsToChars(strcat('2pEcho_ampswp_', pulse_str{1}, '_', pulse_str{2})); % Can add additional info after pulse. Dont cahnge first two to keep structure

    sufficient = 0; % A variable used to end the following loop, when the measurement was sufficient
    runs = 0;

    while (sufficient == 0 && runs < 10 && echo.VideoGain > -31 && echo.VideoGain < 20)   % Loop to execute several measurements if the previous one was insufficient 

        runs = runs + 1;

        launch(echo);

        % finish current experiment loop. Might need update in future. Function
        % which uses disk access availible. File name: Pause_loop_disk_access.m
        
        pause(5);

        while (dig_interface('savenow'))
        
            pause(5);

            desktop = com.mathworks.mde.desk.MLDesktop.getInstance;
            cmdWindow = desktop.getClient('Command Window');
            cmdWindowScrollPane = cmdWindow.getComponent(0);
            cmdWindowScrollPaneViewport = cmdWindowScrollPane.getComponent(0);
            cmdTextUI = cmdWindowScrollPaneViewport.getComponent(0);
            cmdText = cmdTextUI.getText;
            one_string = string(cmdText);
            collum_lines = regexp(one_string, '\n', 'split');
            last_string = collum_lines(end-1);

            displayed_data = sscanf(last_string, 'Ongoing acquisition, %i available and stored. %f to go, which will take some %f minutes');

            try
                if strcmp(last_string, "Acquisition is writing -> No data was saved")|| displayed_data(3) == inf
                    pause(1);
                    continue
                end
            catch
                pause(5);
                continue
            end

            time = displayed_data(3) * 60;    %seconds

            if time < 10
                pause(10);
            elseif time > 600
                pause(600);
            else
                pause(time);
            end    
        end


        % Evaluation function (of maximum)

        % sweep_list = dir('*2pEcho_ampswp_*.mat');
        % a = length(sweep_list);
        % file = sweep_list(a).name;

        file = evalin('base', 'currfilename');

        % test_clipping and only fit if positive

        % Options for uwb_eval
        options.evlen = 256;
        options.phase_all = 1;
        options.plot = 0;

        output = uwb_eval2(file,options);

        dta_cont = output.dta_avg; % averaged echo transients
        dta_ev = output.dta_ev; % echo integrals

        ev_coll = dta_ev;
        dta_x_cont = output.dta_x{1}; % axis of first indirect dimension

        % Functionn to determine maximum(scale)

        real_ev_coll = real(ev_coll);

        % Fit to find a propper maximum/ the scale of the pulse
        [~, M_index] = max(real_ev_coll);
        x_fit = dta_x_cont((M_index - 10):(M_index + 10));
        y_fit = transpose(real_ev_coll((M_index - 10):(M_index + 10)));
        p_fit = polyfit(x_fit, y_fit, 3);
        p_der = polyder(p_fit);
        p_root = roots(p_der);
        [root_value, root_index] = min(abs(p_root - dta_x_cont(M_index)));
        root_wanted = p_root(root_index);

        % Part from Uwbeval, gives best video gain and chenges accordingly
        vid_range = -31:1:20; 
        dig_level_0dB = output.dig_level*10^(-1*echo.VideoGain/20);
        possible_levels = dig_level_0dB .* 10.^(vid_range./20); %conf.std.IFgain_levels / conf.std.IFgain_levels(estr.VideoGain+1);

            % crop the too large levels
        possible_levels(possible_levels > 0.75) = 0;
        [~,best_idx] = max(possible_levels);
        vid_diff = vid_range(best_idx) - echo.VideoGain;
        if vid_diff < 0 || vid_diff > 5
            if runs == 10
                scale = root_wanted;
                warning(['The AMP_swepp for ' pulse ' did not converge within 10 runs.' ... 
                         'The scale is now set to ' root wanted 'dB, which might not be perfect' ...
                         'but the run will be continued']);

            end
            echo.VideoGain = vid_range(best_idx);
        else
            sufficient = 1;
            scale = root_wanted;
        end

    end
    
    figure(fig_amp); clf; hold on;
    plot(dta_x_cont,real(ev_coll),'Color','blue')
%     plot(M_value, root_value,'o-','MarkerSize','5','color','red')
%     legend(legendvec,'Location','best')
    ylabel('Re( Echo integral ) [arb]')
    xlabel('Sweep axis')
    
    VideoGain = echo.VideoGain;
    
    clear reptim;
    reptim.name = 'reptime swp';
    reptim.variables = 'reptime';
    reptim.inc = 0.1e6;
    reptim.strt = 0.5e6;
    reptim.dim = 500;
    
    for i = 1:(length(echo.events)-1)
        echo.events{i}.pulsedef.scale = scale;
    end
    
    echo.parvars = {pcyc,reptim};           echo.savename = '2pecho_reptime';   echo.name = 'T1_Relaxation';
    echo.shots = 10;
    launch(echo);
    
    fig_rt = figure('Name','Reptime sweep before IR');
    
    pause(10);
    while (dig_interface('savenow'))

        pause(5);

        desktop = com.mathworks.mde.desk.MLDesktop.getInstance;
        cmdWindow = desktop.getClient('Command Window');
        cmdWindowScrollPane = cmdWindow.getComponent(0);
        cmdWindowScrollPaneViewport = cmdWindowScrollPane.getComponent(0);
        cmdTextUI = cmdWindowScrollPaneViewport.getComponent(0);
        cmdText = cmdTextUI.getText;
        one_string = string(cmdText);
        collum_lines = regexp(one_string, '\n', 'split');
        last_string = collum_lines(end-1);

        displayed_data = sscanf(last_string, 'Ongoing acquisition, %i available and stored. %f to go, which will take some %f minutes');

        try
            if strcmp(last_string, "Acquisition is writing -> No data was saved")|| displayed_data(3) == inf
                pause(1);
                continue
            end
        catch
            pause(10);
            continue
        end

        time = displayed_data(3) * 60;    %seconds

        if time < 10
            pause(10);
        elseif time > 600
            pause(600);
        else
            pause(time);
        end    
    end
    
    file = evalin('base', 'currfilename');

    % Options for uwb_eval
    options.evlen = 256;
    options.phase_all = 1;
    options.plot = 0;

    % Variablen initialisieren
    dta_cont = cell(length(file),1);
    dta_x_cont = dta_cont;
    ev_coll = dta_cont;
    exp_cont = dta_cont;

    output = uwb_eval2(file,options);

    dta_cont = output.dta_avg; % averaged echo transients
    dta_ev = output.dta_ev; % echo integrals

    ev_coll = dta_ev;
    dta_x_cont = output.dta_x{1}; % axis of first indirect dimension
    
    % First, fit a polynomial of first degree to the last 20% of the data
    
    last_20 = length(dta_x_cont) * 0.8;
    
    equil_model = polyfit(dta_x_cont(last_20:end),transpose(real(ev_coll(last_20:end))/max(real(ev_coll))),1);
    
    equil_y = polyval(equil_model,dta_x_cont);
    
    % Plot
    figure(fig_rt); clf;
    hold on; 
    plot(dta_x_cont,real(ev_coll)/max(real(ev_coll)),'Color','red')
%     legend(legendvec,'Location','best')
    ylabel('Re( Echo integral ) [arb]')
    xlabel('Sweep axis')
    plot(dta_x_cont, equil_y,'Color','blue')
    
    threshhold = 0.985;
    
    if equil_model(1) * 10^7 > 0.01
        warning('Starting reptime might be too short');
    elseif equil_model(2) < 0.985 && equil_model(2) > 0.95 
        threshhold = equil_model(2);
    else
        warning('Starting reptime might be too short');
        threshhold = 0.95;
    end
    
    
    dta_y_cont = transpose(real(ev_coll)/max(real(ev_coll)));
    
    exp_model = @(beta,x)(beta.a*exp(beta.b*x) + beta.c*exp(beta.d*x));
    
    beta = fit(dta_x_cont, dta_y_cont, 'exp2');
    
    results = exp_model(beta,transpose(dta_x_cont));
    
    plot(dta_x_cont, results, 'Color','green');
    
    abs_y_cont = -abs(results - threshhold); % 0.985);
    
    [~ ,reptime] = findpeaks(abs_y_cont);
    
    Hys.myreptime.value = dta_x_cont(reptime);
    if Hys.myreptime.value < Hys.myreptime.min
        Hys.myreptime.value = Hys.myreptime.min;
    end
    changed_myreptime = Hys.myreptime.value;
    
    file_list = dir('*2pecho_reptime*.mat');            % Deleting all but one of the reptime measurements
    if length(file_list) == 1
    else
        for i = 1:(length(file_list)-1)
            delete(file_list(i).name);
        end
    end
    
    % Currently dont delete, might change later
    
end
    
    pause(2);
    clear echo;
    [echo, changed_fields] = get_hahn_echo(Hys, pulse,'AMP sweep');
    if exist('VideoGain','var')
        echo.VideoGain = VideoGain;
    end
    echo.parvars = {pcyc,amp};
    echo.reptime = Hys.myreptime.value;
    echo.shots = Hys.AMP_sweep.shots.value;
    pulse_str = num2cell(string(pulse));
    echo.savename = convertStringsToChars(strcat('2pEcho_ampswp_', pulse_str{1}, '_', pulse_str{2})); % Can add additional info after pulse. Dont cahnge first two to keep structure

    sufficient = 0; % A variable used to end the following loop, when the measurement was sufficient
    runs = 0;

    while (sufficient == 0 && runs < 10 && echo.VideoGain > -31 && echo.VideoGain < 20)   % Loop to execute several measurements if the previous one was insufficient 

        runs = runs + 1;

        launch(echo);

    % finish current experiment loop. Might need update in future. Function
    % which uses disk access availible. File name: Pause_loop_disk_access.m
        pause(10);

        while (dig_interface('savenow'))
        
            pause(5);

            desktop = com.mathworks.mde.desk.MLDesktop.getInstance;
            cmdWindow = desktop.getClient('Command Window');
            cmdWindowScrollPane = cmdWindow.getComponent(0);
            cmdWindowScrollPaneViewport = cmdWindowScrollPane.getComponent(0);
            cmdTextUI = cmdWindowScrollPaneViewport.getComponent(0);
            cmdText = cmdTextUI.getText;
            one_string = string(cmdText);
            collum_lines = regexp(one_string, '\n', 'split');
            last_string = collum_lines(end-1);

            displayed_data = sscanf(last_string, 'Ongoing acquisition, %i available and stored. %f to go, which will take some %f minutes');

            try
                if strcmp(last_string, "Acquisition is writing -> No data was saved")|| displayed_data(3) == inf
                    pause(1);
                    continue
                end
            catch
                pause(10);
                continue
            end

            time = displayed_data(3) * 60;    %seconds

            if time < 10
                pause(5);
            elseif time > 600
                pause(600);
            else
                pause(time - 5);
            end    
        end


        % Evaluation function (of maximum)

        % sweep_list = dir('*2pEcho_ampswp_*.mat');
        % a = length(sweep_list);
        % file = sweep_list(a).name;

        file = evalin('base', 'currfilename');

        % test_clipping and only fit if positive

        % Options for uwb_eval
        options.evlen = 256;
        options.phase_all = 1;
        options.plot = 0;

        output = uwb_eval2(file,options);

        dta_cont = output.dta_avg; % averaged echo transients
        dta_ev = output.dta_ev; % echo integrals

        ev_coll = dta_ev;
        dta_x_cont = output.dta_x{1}; % axis of first indirect dimension

        % Functionn to determine maximum(scale)

        real_ev_coll = real(ev_coll);

        % Fit to find a propper maximum/ the scale of the pulse
        [~, M_index] = max(real_ev_coll);
        x_fit = dta_x_cont((M_index - 10):(M_index + 10));
        y_fit = transpose(real_ev_coll((M_index - 10):(M_index + 10)));
        p_fit = polyfit(x_fit, y_fit, 3);
        p_der = polyder(p_fit);
        p_root = roots(p_der);
        [~, root_index] = min(abs(p_root - dta_x_cont(M_index)));
        root_wanted = p_root(root_index);

        % Part from Uwbeval, gives best video gain and chenges accordingly
        vid_range = -31:1:20; 
        dig_level_0dB = output.dig_level*10^(-1*echo.VideoGain/20);
        possible_levels = dig_level_0dB .* 10.^(vid_range./20); %conf.std.IFgain_levels / conf.std.IFgain_levels(estr.VideoGain+1);

            % crop the too large levels
        possible_levels(possible_levels > 0.75) = 0;
        [~,best_idx] = max(possible_levels);
        vid_diff = vid_range(best_idx) - echo.VideoGain;
        if vid_diff < 0 || vid_diff > 5
            if runs == 10
                scale = root_wanted;
                warning(['The AMP_swepp for ' num2str(pulse) ' did not converge within 10 runs.' ... 
                         'The scale is now set to ' num2str(root_wanted) 'dB, which might not be perfect' ...
                         'but the run will be continued']);

            end
            echo.VideoGain = vid_range(best_idx);
            changed_fields.VideoGain.value = vid_range(best_idx);
        else
            sufficient = 1;
            scale = root_wanted;
        end

    end
    
    figure(fig_amp); clf; hold on;
    plot(dta_x_cont,real(ev_coll),'Color','blue')
%     plot(root_wanted, root_value,'o','MarkerSize','5','color','red')
%     legend(legendvec,'Location','best')
    ylabel('Re( Echo integral ) [arb]')
    xlabel('Sweep axis')
    
    
    if Hys.IR.executed == 0
        changed_fields.myreptime.value = changed_myreptime;
    end
    
    VideoGain = echo.VideoGain;

    if ~exist('clear_files', 'var')
            clear_files = 0;
    end
    switch pulse_str{1}
        case '8'
            if clear_files == 1
                file_list = dir('*2pEcho_ampswp_8_16*.mat');
                if length(file_list) == 1
                else
                    for i = 1:(length(file_list)-1)
                        delete(file_list(i).name);
                    end
                end
            elseif clear_files == 2
                file_list = dir('*2pEcho_ampswp_8_16*.mat');
                file = '';
                for i = 1:(length(file_list))               
                    delete(file_list(i).name);
                end
            end
        case '16'
             if clear_files == 1
                file_list = dir('*2pEcho_ampswp_16_32*.mat');
                if length(file_list) == 1
                else
                    for i = 1:(length(file_list)-1)
                        delete(file_list(i).name);
                    end
                end
            elseif clear_files == 2
                file_list = dir('*2pEcho_ampswp_16_32*.mat');
                file = '';
                for i = 1:(length(file_list))               
                    delete(file_list(i).name);
                end
             end
        otherwise
    end

end



