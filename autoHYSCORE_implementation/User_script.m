%% 
% User script

clear Hyscore;

% % % % % % % % % % % % % % User settings % % % % % % % % % % % % % % % % %

Hyscore.name = '4 pulse Hyscore';

Hyscore.sample = 'Ti_ClAlEt_AAlast';
Hyscore.project = 'Ti_ZNModel';
Hyscore.resonator  = 'MS-3';

% Magnetic field values

Hyscore.b_field.gyro_exp = gyro_exp; % only used if B value is empty. can use e.g. getgyro('TiIII');
Hyscore.b_field.value = [3365 9.40]; % [Bfield freq] or Bfield in Gauss, his value is changed at a later point again
Hyscore.b_field.state = 0;      % 0: approx. start field, to be adjusted, 2: Bfield known precisely: no change

Hyscore.b_field.peaks = [0];    
% 0 is the maximum peak. -1 is the first peak on the left, +1 the first 
% peak on the right (field domaine).
% Example: [-2, 0, 1] will measure Hyscore for 3 peaks, the second one
% with a lower field than the maximum, the maximum and the first one with a
% higher field than the maximum. If the peak is not found it will be skiped
% The following code will find the peaks. You might want to check that
% these peaks are found if you have a field sweep at hand:
% [peaks_found, peaks_ind] = findpeaks(real(dta_ev)/max(real(dta_ev)),dta_x,...
%   'MinPeakProminence',0.01, 'MinPeakHeight',0.1);

Hyscore.temp.try_values = 15; % temperature in K, can be array


Hyscore.HYSCORE.n_taus = 3;     % Number of taus for Hyscore measurement
Hyscore.HYSCORE.snr = 10;      % minimum acceptable signal/noise ratio of first Hyscore
Hyscore.HYSCORE.min_shots = 4;  % min. number of shots acceptable for Hyscore

Hyscore.max_runtime = 2.7 * 24;    % hrs     % The maximum time for the code to run <-> how much measurement time you have
Hyscore.finish_early = 0;   % 0: the entire runtime is used for best S/N. 1: finish at target S/N (if before max_runtime)


Hyscore.clear_files.AMP_sweep = 0;
Hyscore.clear_files.IR = 0;
Hyscore.clear_files.nut = 0;

Hyscore.save_directory.ESEEM_3p = '3pESEEM_data_final';
Hyscore.save_directory.ESEEM_4p = '4pESEEM_data_final';
Hyscore.save_directory.HYSCORE = '4pHYSCORE_data_final';    % B value will be added to folder name


% % % % % % % % % % % % % % Advanced % % % % % % % % % % % % % % % % % % %

Hyscore.VideoGain.value = 0; % dB


Hyscore.myreptime.value = 5e7;
Hyscore.myreptime.min = 1e6;

Hyscore.shots_std.value = 50;

Hyscore.myLO.value = 7.8;


Hyscore.res_frequ.value = 9.36;


Hyscore.IR.executed = 0;

Hyscore.store_avgs.value = 0;

Hyscore.det_len.value = 512;

Hyscore.avgs.value = 1.0;

Hyscore.AMP_sweep.state = 0;



Hyscore.nu_obs.value = Hyscore.res_frequ.value - Hyscore.myLO.value;



Hyscore.tau.echo = 200;
Hyscore.tau.res_profile = 600;
Hyscore.tau.IR = 600;                   % Might just want to keep 1 of these 3
Hyscore.tau.nut = 600;              
Hyscore.tau.b_sweep = 200;              % Might just keep tau.echo


Hyscore.obs_delay.res_profile = 1000;
Hyscore.obs_delay.IR = 1000;            % Might just want to keep 1 of these 3
Hyscore.obs_delay.nut = 1000;


Hyscore.AMP_sweep.shots.factor = 1;
Hyscore.AMP_sweep.shots.value = Hyscore.shots_std.value * Hyscore.AMP_sweep.shots.factor;
Hyscore.AMP_sweep.shots.state = 0;


Hyscore.b_sweep.shots.factor = 1;
Hyscore.b_sweep.shots.value = Hyscore.shots_std.value * Hyscore.b_sweep.shots.factor;
Hyscore.b_sweep.shots.state = 0;


Hyscore.echo.shots.factor = 2;
Hyscore.echo.shots.value = Hyscore.shots_std.value * Hyscore.echo.shots.factor;
Hyscore.echo.shots.state = 0;


Hyscore.IR.shots.factor = 0.2;
Hyscore.IR.shots.value = Hyscore.shots_std.value * Hyscore.IR.shots.factor;
Hyscore.IR.shots.state = 0;


Hyscore.res_profile.shots.factor = 1;
Hyscore.res_profile.shots.value = Hyscore.shots_std.value *Hyscore.res_profile.shots.factor;
Hyscore.res_profile.shots.state = 0;


Hyscore.ESEEM2p.tau = 200;
% Hyscore.ESEEM2p.det_len = 512;
Hyscore.ESEEM2p.shots.factor = 0.4;
Hyscore.ESEEM2p.shots.value = Hyscore.shots_std.value * Hyscore.ESEEM2p.shots.factor;


Hyscore.ESEEM3p.tau_start = 128;
Hyscore.ESEEM3p.tau_step = 8; 
Hyscore.ESEEM3p.tau_n = 16;         % This is the number of taus for which we take the 3pESEEM
Hyscore.ESEEM3p.tau_end = Hyscore.ESEEM3p.tau_start + Hyscore.ESEEM3p.tau_step * (Hyscore.ESEEM3p.tau_n - 1);
Hyscore.ESEEM3p.shots.factor = 2;
Hyscore.ESEEM3p.shots.value = Hyscore.shots_std.value * Hyscore.ESEEM3p.shots.factor;
% Hyscore.ESEEM3p.det_len = 512;
Hyscore.ESEEM3p.trace_n = Hyscore.HYSCORE.n_taus;    % This is the number of tau values for which we want to record the HYSCORE -> tau_n >= trace_n


% Hyscore.ESEEM4p.tau = 208;  % ns
% Hyscore.ESEEM4p.T1 = 208;   % ns
Hyscore.ESEEM4p.shots.factor = 1;
Hyscore.ESEEM4p.shots.value = Hyscore.shots_std.value * Hyscore.ESEEM4p.shots.factor;
Hyscore.ESEEM4p.det_len = 512;
Hyscore.ESEEM4p.reptime_stepsize = 0.5*1e6;
Hyscore.ESEEM4p.T_2swp_dim = 256; 
Hyscore.ESEEM4p.T2_swp.inc = 16 * 1.111111111;
Hyscore.ESEEM4p.T2_swp.strt = 200;    


Hyscore.HYSCORE.T1_swp.inc = 16 * 1.111111111;  
Hyscore.HYSCORE.T1_swp.dim = 256;
Hyscore.HYSCORE.T1_swp.strt = 200;
Hyscore.HYSCORE.T2_swp.inc = 16 * 1.111111111;
Hyscore.HYSCORE.T2_swp.dim = 256;
Hyscore.HYSCORE.T2_swp.strt = 200;
% Hyscore.HYSCORE.field_vec = [3365];
% Hyscore.HYSCORE.shots.value = 20;

switch Hyscore.resonator
    case 'MS-3'
        Hyscore.starting_scale = 0.1;
    otherwise
        Hyscore.starting_scale = 1;
end


%% Not to be touched by user


%Clear file states for different experiments (only make it one parameter?)

% Hyscore.p_pi2_16 = p90_16;
% Hyscore.p_pi_32 = p180_32;
% Hyscore.p_pi_x = p180_x;
% Hyscore.hard = hard;
% 
% pulse_fields(1) = Hyscore.p_pi2_16;
% pulse_fields(2) = Hyscore.p_pi2_16;
% pulse_fields(3) = Hyscore.p_pi_x;
% pulse_fields(4) = Hyscore.p_pi2_16;
% 
% Hyscore.taus = [200, 208, 250];