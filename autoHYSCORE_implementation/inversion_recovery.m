% Inversion Recovery experiment to determine T1

% Test

function [reptime,file] = inversion_recovery(Hys, pulse_fields, clear_files)

tau = Hys.tau.IR;

clear pcyc;
pcyc.name = 'o1.2';
pcyc.variables = {'events{2}.pulsedef.phase','events{4}.det_sign'};
pcyc.ax_id = 0;
pcyc.vec = [0 1; pi -1];
pcyc.reduce = 1;

clear obs_delay_incr
obs_delay_incr.name = 'obs_delay linear increase';
obs_delay_incr.variables = {'events{2}.t','events{3}.t','events{4}.t'};
obs_delay_incr.strt = [400 400+tau 400+2*tau]; 
obs_delay_incr.inc = 25000*[1 1 1].*1.11111111111;
obs_delay_incr.dim = 1024;

max_sequence_length = obs_delay_incr.dim*obs_delay_incr.inc(1);

% options for uwb_eval2
clear options;
options.plot = 0;
options.evlen = 256;
options.phase_all = 0;

clear fstepnut;
fstepnut = get_fstepnut_IR(Hys,pulse_fields);

fstepnut.parvars = {pcyc, obs_delay_incr};

fstepnut.savename = ['IR_linear_incr']; % Can add additional info after pulse. Dont cahnge first two to keep structure

fstepnut.reptime = 1.1 * max_sequence_length;
    
    
    launch(fstepnut);

% finish current experiment loop. Might need update in future. Function
% which uses disk access availible. File name: Pause_loop_disk_access.m
    pause(5);

    while (dig_interface('savenow'))
        
        pause(5);

        desktop = com.mathworks.mde.desk.MLDesktop.getInstance;
        cmdWindow = desktop.getClient('Command Window');
        cmdWindowScrollPane = cmdWindow.getComponent(0);
        cmdWindowScrollPaneViewport = cmdWindowScrollPane.getComponent(0);
        cmdTextUI = cmdWindowScrollPaneViewport.getComponent(0);
        cmdText = cmdTextUI.getText;
        one_string = string(cmdText);
        collum_lines = regexp(one_string, '\n', 'split');
        last_string = collum_lines(end-1);
    
        displayed_data = sscanf(last_string, 'Ongoing acquisition, %i available and stored. %i to go, which will take some %f minutes');
        
        try
            if strcmp(last_string, "Acquisition is writing -> No data was saved")|| displayed_data(3) == inf
                pause(1);
                continue
            end
        catch
            pause(10);
            continue
        end
            
        time = displayed_data(3) * 60;    %seconds
    
        if time < 10
            pause(5);
        elseif time > 600
            pause(600);
        else
            pause(time - 5);
        end    
    end


    % Evaluation function (of maximum)

    file = evalin('base', 'currfilename');

    % Options for uwb_eval
    options.evlen = 256;
    options.phase_all = 1;
    options.plot = 0;

    % Variablen initialisieren
    dta_cont = cell(length(file),1);
    dta_x_cont = dta_cont;
    ev_coll = dta_cont;
    exp_cont = dta_cont;

    output = uwb_eval2(file,options);

    dta_cont = output.dta_avg; % averaged echo transients
    dta_ev = output.dta_ev; % echo integrals

    ev_coll = dta_ev;
    dta_x_cont = output.dta_x{1}; % axis of first indirect dimension
    
    % First, fit a polynomial of first degree to the last 20% of the data
    
    last_20 = round(length(dta_x_cont) * 0.8);
    [~,minimum] = min(real(ev_coll));
    
    equil_model = polyfit(dta_x_cont(last_20:end),transpose(real(ev_coll(last_20:end))/max(real(ev_coll))),1);
    
    equil_y = polyval(equil_model,dta_x_cont(minimum:end));
    
    % Plot
    figure('Name','Inversion recovery experiment'); clf; hold on;
    hold on; 
    plot(dta_x_cont(minimum:end),real(ev_coll(minimum:end))/max(real(ev_coll)),'Color','red')
%     legend(legendvec,'Location','best')
    ylabel('Re( Echo integral ) [arb]')
    xlabel('Sweep axis')
    plot(dta_x_cont(minimum:end), equil_y,'Color','blue')
    
    threshhold = 0.985;                                     % -> Updated for version 1.1, bug fix of wrong reptime
    
    if equil_model(1) * 10^7 > 0.01
        warning('Starting reptime might be too short');
    elseif equil_model(2) < 0.985 && equil_model(2) > 0.95 
        threshhold = equil_model(2);
    elseif equil_model(2) > 0.985
        threshhold = 0.985;
    elseif equil_model(2) < 0.95
        threshhold = 0.95;
    end
    
    dta_y_cont = transpose(real(ev_coll(minimum:end))/max(real(ev_coll)));
    
    exp_model = @(beta,x)(beta.a*exp(beta.b*x) + beta.c*exp(beta.d*x));
    
    beta = fit(dta_x_cont(minimum:end), dta_y_cont, 'exp2');
    
    results = exp_model(beta,transpose(dta_x_cont(minimum:end)));
    
    plot(dta_x_cont(minimum:end), results, 'Color','green');
    
    abs_y_cont = -abs(results - threshhold); % 0.985);
    
    [~ ,reptime] = findpeaks(abs_y_cont);

    reptime = reptime + minimum;
    
    reptime = dta_x_cont(reptime);
    
    plot(reptime,[exp_model(beta,reptime)],'o','MarkerSize',5,'color','Black')

    legend('Data','Equilibrium','Modell', 'Result','Location','best')


if ~exist('clear_files', 'var')
    clear_files = 0;
end

if clear_files == 1
    file_list = dir('*IR_linear_incr*.mat');
    if length(file_list) == 1
    else
        for i = 1:(length(file_list)-1)
            delete(file_list(i).name);
        end
    end
elseif clear_files == 2
    file_list = dir('*IR_linear_incr*.mat');
    file = '';
    for i = 1:(length(file_list))               
        delete(file_list(i).name);
    end
end


end