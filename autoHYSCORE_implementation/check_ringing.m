function [conf_exp] = check_ringing(conf_exp,resonator,chirper)
    
    switch resonator
        case 'MS-3'
            min_delay = 30;     % ns
        otherwise
            min_delay = 200;    % ns
    end
    
    chirper.shots = 20;
    chirper.B = 500;    % G
    chirper.VideoGain = 0;      %dB
    chirper.events{length(chirper.events)}.det_pos = 0;
    chirper.events{length(chirper.events)}.det_len = 8192;
    
    tau = chirper.events{length(chirper.events)}.t - chirper.events{length(chirper.events)-1}.t;
    
    current_delay = conf_exp.std.exc_trigX.delays(1,2);
    final_delay = current_delay;
    
    ringing = 0;
    
    % DAKL: other settings missing here for chirper: shots, det_len, etc.
    chirper_0 = chirper;
    for i = 1:length(chirper.events)-1 % DAKL -1 added: last event(det) doesn't have pulsedef
        chirper_0.events{i}.pulsedef.scale = 0;
    end
    
    % Initialize figures
    
    fig_RC0 = figure('Name',['Ringing Check 0 env ' num2str(length(chirper.events)-1) 'P']);
    fig_RC = figure('Name',['Ringing Check ' num2str(length(chirper.events)-1) 'P']);
    
    if current_delay >= min_delay
        finished = 0;
        runs = 0;
        max_runs = (current_delay - min_delay)/10 + 1;
        while ~ringing && (current_delay >= min_delay) && finished == 0
            
            runs = runs + 1;
            
            % careful!!!
            EXdelays = conf_exp.std.exc_trigX.delays;
            EXdelays(1,:) = [-1400 current_delay]; % minimum: [-1400 (68 or 30)]; % minimum MS-3 & ScoI: [-1400 30]; % DEFENSE default [-1400 120]; %%% no ringing visible
            % EXdelays(3,:) = [-1300 -130]; % TWTgate default -1300 -130 or -80 (18.08.2020) % cutting at -160
            conf_exp.std.exc_trigX.delays = EXdelays;

            pause(3);
            
            launch(chirper_0, conf_exp); % launch overwrites conf in base workspace
            conf_exp.act = evalin('base','conf.act');
            
%             brk_interface('wait');
            brk_interface('wait');
            pause(5);
            
            dta_0 = double(dig_interface('get'));
            
            [env_up_0, env_low_0] = envelope(dta_0,200,'rms');
            max_env_0 = max(abs([env_up_0; env_low_0]));
            
            figure(fig_RC0); clf; hold on;
            plot(dta_0,'b')
            plot(env_up_0,'r')
            plot(env_low_0,'r')
            
            if runs == 1
                const_line = ones(length(env_up_0),1)*max_env_0*0.5;
                x_0 = transpose(1:length(env_up_0));
                plot(const_line,'g')
                X = intersections(x_0,env_up_0,x_0,const_line);
                
                exp_intersect = X(end);                         % all measured in data points
                theo_intersect = 8192/2 - tau*1e-9*1.8e9;
                dif_intersect = exp_intersect-theo_intersect;
                
                if dif_intersect < - 30 || dif_intersect > 100
                    warning('The signal seems to be in the wrong place. You might need to restart the ADQ/Matlab.')
                    conf_exp = -1;
                    return;
                end
            end
            
            pause(3);
            
            launch(chirper, conf_exp); % launch overwrites conf in base workspace
            conf_exp.act = evalin('base','conf.act');
            
            brk_interface('wait');
            pause(5);
            
            dta = double(dig_interface('get'));
            
            [env_up, env_low] = envelope(dta,200,'rms');
            
            figure(fig_RC); clf; hold on;
            plot(dta,'b')
            plot(env_up,'r')
            plot(env_low,'r')
            
            dif_env = [env_up-env_up_0; -(env_low-env_low_0)];
            
            
            for i = 1:length(dif_env)
                if dif_env(i) > max_env_0 * 0.35                % Threshhold
                    ringing = 1;
                    if runs == 1
                        warning('Ringing was detected at your current delay and the experiment will be abborted')
                        conf_exp = -1;
                        return;
                    else
                        warning(['Ringing was detected at a delay of ' num2str(current_delay) ' ns.  A delay of ' num2str(final_delay) ' ns will be used for the rest of the run'])
                        
                        % !! Carefull
                        EXdelays(1,:) = [-1400 final_delay];
                        conf_exp.std.exc_trigX.delays = EXdelays;
                    end
                    
                end
            end
            if ringing == 0
                final_delay = current_delay;
                if current_delay == min_delay 
                    finished = 1;
                    % !! Carefull
                    EXdelays(1,:) = [-1400 final_delay];
                    conf_exp.std.exc_trigX.delays = EXdelays;
                    
                elseif current_delay - 10 < min_delay
                    current_delay = min_delay;
                else
                    current_delay = current_delay - 10;
                end
            end
        end
    else % meaning here: elseif current_delay < min_delay
        
        error(['Minimum delay violation in ringing test! Delay ' num2str(current_delay) ' attempted. Abborting.']);
        
        % execution below should never happen: pulses made with too short
        % protection delay
        
        EXdelays = conf_exp.std.exc_trigX.delays;
        EXdelays(1,:) = [-1400 min_delay]; % minimum: [-1400 (68 or 30)]; % minimum MS-3 & ScoI: [-1400 30]; % DEFENSE default [-1400 120]; %%% no ringing visible
        % EXdelays(3,:) = [-1300 -130]; % TWTgate default -1300 -130 or -80 (18.08.2020) % cutting at -160
        conf_exp.std.exc_trigX.delays = EXdelays;
        
        pause(3);
        
        launch(chirper_0, conf_exp); % launch overwrites conf in base workspace
        conf_exp.act = evalin('base','conf.act');
        
%         brk_interface('wait');
        brk_interface('wait');
        pause(5);

        dta_0 = double(dig_interface('get'));

        [env_up_0, env_low_0] = envelope(dta_0,200,'rms');
        figure(fig_RC0); clf; hold on;
        plot(dta_0,'b')
        plot(env_up_0,'r')
        plot(env_low_0,'r')
        
        pause(3);
        
        launch(chirper, conf_exp); % launch overwrites conf in base workspace
        conf_exp.act = evalin('base','conf.act');

%         brk_interface('wait');
        brk_interface('wait');
        pause(5);

        dta = double(dig_interface('get'));

        [env_up, env_low] = envelope(dta,200,'rms');

        figure(fig_RC); clf; hold on;
        plot(dta,'b')
        plot(env_up,'r')
        plot(env_low,'r')

        max_env_0 = max(abs([env_up_0; env_low_0]));

        dif_env = [env_up-env_up_0; -(env_low-env_low_0)];
        
        for i = 1:length(dif_env)
            if dif_env(i) > max_env_0 * 0.35
                warning('Ringing was detected at your current delay and the experiment will be abborted')
                conf_exp = -1;
                return;
            end
        end
%         warning('Your starting delay seems to be at or below the minimum delay for your resonator. If you think a smaller delay should be possible, please contact the system admin')
    end
end

