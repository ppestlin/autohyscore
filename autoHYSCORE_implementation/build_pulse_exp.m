function wave = build_pulse_exp( chirp, options )
%BUILD_PULSE_EXP is the function which calculates pulses for the AWG. The
%chirp field describes the chirp pulse and its contents are determined by
%the pulse type. The pulses provided to BUILD_PULSE_EXP are presumed to be
%conditioned by pulse_standardize(), which is the natural procedure for the
%launch() function. Other helper functions, such as
% For the options:
% options.awg           Structure with the AWG, not really optional here
% options.LO            The LO frequency of the experiment. That may be
%                       used to adjust the frequency axis of a resonator
%                       profile for pulse compensation. This, however,
%                       requires that the resonator itself provides its LO
%                       frequency
%
%As of 2016, this function has a similar name and input/output than
%build_pulse used in SPIDYAN. The actual implementation here has a few
%differences as compared to SPIDYAN. Besides different options being
%available in SPIDYAN (for instance labframe and LOframe) as compared to
%the experiment, there are no fundamental differences in calculated pulses.
%   And a historical note: The ouputs from build_pulse/build_pulse_exp have
%   their data stored in the second dimension, whereas most routines
%   related to experiments on the AWG spectrometer have the data stored in
%   the first dimension. In any case, during compilation with seq_map, the
%   waveforms are reshaped into the required form, where data is in the
%   first dimension.



% first, make substitions which need to be done here
% general input checks of the fields should not go here, but in
% exp_standardize, since build_pulse_exp may be called thousands of times
% to compute an experiment. Repeated input checking is thus expensive.
if ~isfield(chirp,'phase'), % in-phase phase (CH1)
    chirp.phase=0;
end;
if ~isfield(chirp,'Qphase'), % quadrature phase (CH2)
    chirp.Qphase=chirp.phase;
end;
if isfield(chirp,'Qshift'), % quadrature phase correction (CH2)
    chirp.Qphase=chirp.Qphase+chirp.Qshift;
end;
if ~isfield(chirp,'Qscale'), % quadrature amplitude correction (CH2)
    chirp.Qscale=1;
end;
if ~isfield(chirp,'Iscale'), % in-phase amplitude correction (CH2)
    chirp.Iscale=1;
end;

% discretization (time and amplitude)
awg = options.awg;
res=2^awg.vert_res; % vertical resolution in steps
wave.dt=1/awg.s_rate; % time resolution
n=ceil(chirp.tp/wave.dt); % number of points
wave.tp=n*wave.dt;
t=0:wave.dt:wave.tp-wave.dt; % time axis

%  precheck for resonator and eventual shifting of its frequency axis
resonatorcomp = 0;
if isfield(chirp,'resonator') 
    resonatorcomp = 1;
    % check for eventual reshifting of the resonatorprofile range
    if isfield(chirp.resonator,'LO') && isfield(options,'LO') && chirp.resonator.LO ~= options.LO
        chirp.resonator.range = chirp.resonator.range + chirp.resonator.LO - options.LO;
    end
    nu1_MHz = 0;
    % check for resonator nu1 in MHz
    if max(abs(chirp.resonator.nu1)) > 1
        nu1_MHz = 1;
    end
end

% This will be the cos/sin argument due to FM
FM_arg = [];

% get AM and FM of the pulse
%% linear sweeps
if strcmp(chirp.type,'chirp') || strcmp(chirp.type,'WURST') || strcmp(chirp.type,'Gausscascade') || strcmp(chirp.type,'Fourier')
    
    % FM
    % monochromatic or frequency-swept?
    if ~isfield(chirp,'nu_final')
        FM = ones(1,n)*chirp.nu_init;
        FM_arg = 2*pi*chirp.nu_init*t; % speedup for monochromatic pulses (avoids expensive cumtrapz)
    else
        FM = linspace(chirp.nu_init,chirp.nu_final,n);
    end;
    
    % AM
    if strcmp(chirp.type,'chirp')
        % amplitude modulation for t_rise pulse
        AM = ones(1,n);
        if chirp.t_rise ~= 0
            nr=round(chirp.t_rise/wave.dt); % number of points to smooth
            dr=nr*wave.dt;
            tr=dr:-wave.dt:0;
            edge=cos(pi*tr/(2*dr));
            % check if t_rise beyond pulse length
            % (in the experiment we do allow for t_rise > t_p/2)
            % (that is just a more smoothened pulse)
            if nr+1 < n
                AM(1:nr+1) = edge.*AM(1:nr+1);
                AM(end-nr:end) = fliplr(edge) .* AM(end-nr:end);
            else
                error(['t_rise of ' num2str(chirp.t_rise) ' is longer than pulse duration of ' num2str(chirp.tp) ' ns. Fix that!']);
            end
        end
    elseif strcmp(chirp.type,'WURST')
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %
        %
        %   there is no pulse. struct.... bug?
        %
        %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % WURST amplitude modulation
        tcent = pulse.tp / 2; % centertime
        AM = 1-abs(sin((pi*(t-tcent))/pulse.tp)).^pulse.WURSTN; % WURST
    elseif strcmp(chirp.type,'Gausscascade')
        AM = zeros(1,n);
        %sum over Gauss cascade components
        for i_gauss=1:numel(chirp.A_n)
            AM=AM+chirp.A_n(i_gauss)*exp(-4*log(2)/((chirp.deltax_n(i_gauss)*wave.tp)^2)*(t-wave.tp*chirp.x_n(i_gauss)).^2          );
        end
        AM=AM/max(abs(AM));
        
    elseif strcmp(chirp.type,'Fourier')
        omega=2*pi/wave.tp;
        AM = ones(1,n)*chirp.A_n(1);
        %sum over fourier components (ii=1 is A0, since we can't count from 0....)
        for ii=2:numel(chirp.A_n)
            AM=AM+chirp.A_n(ii)*cos((ii-1)*omega*t)+chirp.B_n(ii)*sin((ii-1)*omega*t);
        end
        AM=AM/max(abs(AM));
    end
    
    % PM phase modulation (phase-shift put on top of FM)
    PM = zeros(1,n);
    neg_amps=find(AM<0);
    if ~isempty(neg_amps)
      PM(neg_amps)=180;
      AM(neg_amps)=-AM(neg_amps);
    end
    
    % resonator compensation for linear sweeps: adapt FM, preserve AM
    if resonatorcomp
        % approximation of time-domain amplitude modulation by resonator
        nu1_t = interp1(chirp.resonator.range,abs(chirp.resonator.nu1),FM,'pchip');
        
        % adiabaticity: negative for downsweeps!
        wave.qref = 2*pi*t(end) / trapz(FM,1./nu1_t.^2);
        if nu1_MHz
            wave.qref = wave.qref * 0.001^2; % convert nu1 in MHz to GHz
        end
        
        if ~isinf(wave.qref) % infinity is reached for nu_init == nu_final
            % time spent per frequency
            dt_df = wave.qref./(2*pi*nu1_t.^2);
            % time t(f) from dt/df
            t_f = cumtrapz(FM,dt_df);
            % reparametrize t(f) to FM f(t)
            FM = interp1(t_f,FM,t,'pchip');
        end
        
        % keep the feature of adding resonator phase onto pulse, even
        % though it is currently used rather rarely
        if ~isreal(chirp.resonator.nu1(1))
            PM = PM + interp1(chirp.resonator.range,angle(chirp.resonator.nu1),FM,'pchip');
        end
    end
    
    
    %% hyperbolic secant
elseif strcmp(chirp.type,'HS')
    
    deltaf = chirp.nu_final - chirp.nu_init;
    tcent = chirp.tp / 2;
    
    % AM
    beta = chirp.HSbeta;
    order = chirp.HSorder;
    beta_exp = log(beta*(0.5)^(1-order))/log(beta); % that is due to the -0.5...0.5 time axis used here (others use -1...1) 
    cut = round(length(t)/2);
    AM(1:cut) = sech(beta^beta_exp *((t(1:cut)-tcent)/chirp.tp).^order); % sech
    order = chirp.HSorder2;
    beta_exp = log(beta*(0.5)^(1-order))/log(beta);
    AM(cut+1:length(t)) = sech(beta^beta_exp * ((t(cut+1:end)-tcent)/chirp.tp).^order);
    
    % FM
    FM = deltaf*cumtrapz(t,AM.^2)/trapz(t,AM.^2) + chirp.nu_init;
    
    % PM phase modulation (phase-shift put on top of FM)
    PM = zeros(1,n);
    
    % resonator compensation for linear sweeps: adapt FM and AM
    if resonatorcomp
        % approximation of time-domain amplitude modulation by resonator
        nu1_t = interp1(chirp.resonator.range,abs(chirp.resonator.nu1),FM,'pchip');
        % net AM: digital AM and AM by resonator
        nu1_t = nu1_t .* AM; FM_orig = FM;
        
        % adiabaticity: negative for downsweeps!
        wave.qref = 2*pi*t(end) / trapz(FM,1./nu1_t.^2);
        if nu1_MHz
            wave.qref = wave.qref * 0.001^2; % convert nu1 in MHz to GHz
        end
        
        if ~isinf(wave.qref) % infinity is reached for nu_init == nu_final
            % time spent per frequency
            dt_df = wave.qref./(2*pi*nu1_t.^2);
            % time t(f) from dt/df
            t_f = cumtrapz(FM,dt_df);
            % reparametrize t(f) to FM f(t)
            FM = interp1(t_f,FM,t,'pchip');
            
            % reparametrize digital AM to new FM
            AM = interp1(FM_orig,AM,FM,'pchip');
        end
        
        % keep the feature of adding resonator phase onto pulse, even
        % though it is currently used rather rarely
        if ~isreal(chirp.resonator.nu1(1))
            PM = PM + interp1(chirp.resonator.range,angle(chirp.resonator.nu1),FM,'pchip');
        end
    end
    
elseif strcmp(chirp.type,'FM_mod')
    %no amplitude and frequency modulation
    AM = ones(1,n);
    PM = 0*AM;
    FM = chirp.nu_init+chirp.nu_2*sin(2*pi*chirp.nu_m*t+chirp.phi_m);
    if chirp.nu_m==0
        FM_arg = 2*pi*(chirp.nu_init*t);
    else
        FM_arg = 2*pi*(chirp.nu_init*t - chirp.nu_2/(2*pi*chirp.nu_m)*(cos(2*pi*chirp.nu_m*t+chirp.phi_m)-cos(chirp.phi_m)));
    end
    
elseif strcmp(chirp.type,'composite2')
    
    
    %no amplitude and frequency modulation
    AM = zeros(1,n);
    FM = ones(1,n)*chirp.nu_init;
    FM_arg = 2*pi*chirp.nu_init*t; % speedup for monochromatic pulses (avoids expensive cumtrapz)
    
    
    PM =0*AM;
    
    current=1;
    n_pulse =ceil(chirp.tp1/wave.dt);
    PM(current+(0:(n_pulse-1))) = chirp.phase1;
    AM(current+(0:(n_pulse-1))) = chirp.scale1;
    
    nsmooth = ceil(chirp.tsmooth/wave.dt);
    rel_form = normpdf(0:(nsmooth-1),0,nsmooth/3);
    rel_form = rel_form/max(rel_form);
    AM(current+(n_pulse-nsmooth)+(0:(nsmooth-1))) = chirp.scale2+rel_form*(chirp.scale1-chirp.scale2);
    
    current=current+n_pulse;
    
    n_pulse =ceil(chirp.tp2/wave.dt);
    PM(current+(0:(n_pulse-1))) = chirp.phase2;
    AM(current+(0:(n_pulse-1))) = chirp.scale2;
%     current=current+n_pulse;
    
    
    PM=mod(PM,2*pi);   
    
elseif strcmp(chirp.type,'composite3')
    
    
    %no amplitude and frequency modulation
    AM = zeros(1,n);
    FM = ones(1,n)*chirp.nu_init;
    FM_arg = 2*pi*chirp.nu_init*t; % speedup for monochromatic pulses (avoids expensive cumtrapz)
    
    
    PM =0*AM;
    
    %first pulse
    current=1;
    n_pulse =ceil(chirp.tp1/wave.dt);
    PM(current+(0:(n_pulse-1))) = chirp.phase1;
    AM(current+(0:(n_pulse-1))) = chirp.scale1;

    %smooth of start
    nsmooth = ceil(chirp.tsmooth_start/wave.dt);
    rel_form = normpdf(-(nsmooth-1):0,0,nsmooth/3);
    rel_form = rel_form/max(rel_form);
    AM(current+(0:(nsmooth-1))) = rel_form*(chirp.scale1);
    %smooth of end
    nsmooth = ceil(chirp.tsmooth_12/wave.dt);
    rel_form = normpdf(0:(nsmooth-1),0,nsmooth/3);
    rel_form = rel_form/max(rel_form);
    AM(current+(n_pulse-nsmooth)+(0:(nsmooth-1))) = chirp.scale2+rel_form*(chirp.scale1-chirp.scale2);
    current=current+n_pulse;
    
    %second pulse
    n_pulse =ceil(chirp.tp2/wave.dt);
    PM(current+(0:(n_pulse-1))) = chirp.phase2;
    AM(current+(0:(n_pulse-1))) = chirp.scale2;
  
    nsmooth = ceil(chirp.tsmooth_23/wave.dt);
    rel_form = normpdf(0:(nsmooth-1),0,nsmooth/3);
    rel_form = rel_form/max(rel_form);
    AM(current+(n_pulse-nsmooth)+(0:(nsmooth-1))) = chirp.scale3+rel_form*(chirp.scale2-chirp.scale3);
    current=current+n_pulse;
    
    n_pulse =ceil(chirp.tp3/wave.dt);
    PM(current+(0:(n_pulse-1))) = chirp.phase3;
    AM(current+(0:(n_pulse-1))) = chirp.scale3;
    
    
    PM=mod(PM,2*pi); 
    
    PM = PM(1:n);
    AM = AM(1:n);
    
elseif strcmp(chirp.type,'PEANUT')
    
    % check shift is physically possible
    
    if(chirp.shift > (chirp.tp)/2)
        
        error(['shift is larger than half of the pulse duration'])
        
    end
    
    %check if (n_pulse-1) is dividable by 2
    if ~(mod(n,2) == 0)
        n = n - 1;  
        t = t(1:end-1);
        wave.tp = t(end);
    end
    
    %no amplitude and frequency modulation
    AM = ones(1,n);
    FM = ones(1,n)*chirp.nu_init;
    FM_arg = 2*pi*chirp.nu_init*t; % speedup for monochromatic pulses (avoids expensive cumtrapz)
    
    
    PM =0*AM; % preallocation of PM vector
    

    n_shift =ceil(chirp.shift/wave.dt);
    
    
    PM(1:(n/2)+n_shift) = 0;
    PM(((n/2)+n_shift+1):end) = pi;
    

    PM=mod(PM,2*pi);

    
elseif strcmp(chirp.type,'p_d_train')    
    AM = zeros(1,n);
    FM = ones(1,n)*chirp.nu_init;
    FM_arg = 2*pi*chirp.nu_init*t; % speedup for monochromatic pulses (avoids expensive cumtrapz)
    PM =0*AM;
    
    n_pulse =ceil(chirp.p/wave.dt);
    n_delay =ceil(chirp.d/wave.dt);
    
    current = 1;
    
    for ii = 1:chirp.N
       AM(current+(0:(n_pulse-1))) = 1;
       current =current + n_delay + n_pulse;
    end  
    
    
elseif strcmp(chirp.type,'XiX_train')
    AM = zeros(1,n);
    FM = ones(1,n)*chirp.nu_init;
    FM_arg = 2*pi*chirp.nu_init*t; % speedup for monochromatic pulses (avoids expensive cumtrapz)
    PM =0*AM;
    

    
    current = 1;
    
    % first pulse
    if isfield(chirp,'tp_initial')
    n_init = round(chirp.tp_initial/wave.dt);
    AM(current+(0:(n_init-1))) = chirp.scale_initial;
    PM(current+(0:(n_init-1))) = chirp.phase_initial;
    current =current + n_init;
    else
        n_init=0;
    end
    
    if isfield(chirp,'tp_end')
        n_end = round(chirp.tp_initial/wave.dt);
    else
        n_end=0;
    end
       
    % train
    n1 =round(chirp.tp1/wave.dt);
    n2 =round(chirp.tp2/wave.dt);
    Ncyc = floor((n-n_init-n_end)/(n1+n2));
    
    if Ncyc>0
    
    for ii = 1:Ncyc
       AM(current+(0:(n1-1))) = chirp.scale1;
       PM(current+(0:(n1-1))) = chirp.phase1;
       current =current + n1;
       
       AM(current+(0:(n2-1))) = chirp.scale2;
       PM(current+(0:(n2-1))) = chirp.phase2;
       current =current + n2;
    end 
    
    end
    
    if isfield(chirp,'tp_end') 
        if chirp.tp_end>0
        AM(current+(0:(n_end-1))) = chirp.scale_end;
        PM(current+(0:(n_end-1))) = chirp.phase_end;
        end
    end
    
elseif strcmp(chirp.type,'XiX_train_2')
    AM = zeros(1,n);
    FM = ones(1,n)*chirp.nu_init;
    FM_arg = 2*pi*chirp.nu_init*t; % speedup for monochromatic pulses (avoids expensive cumtrapz)
    PM =0*AM;
   
    current = 1;
    
    % train
    n1 =round(chirp.tp1/wave.dt);
    n2 =round(chirp.tp2/wave.dt);
    Ncyc = floor((n)/(n1+n2));
    
    if Ncyc>0
    
    for ii = 1:Ncyc
       
        % +x
       AM(current+(0:(n1-1))) = chirp.scale1;
       PM(current+(0:(n1-1))) = chirp.phase1;
       current =current + n1;
       
       % -x
       AM(current+(0:(n2-1))) = chirp.scale2;
       PM(current+(0:(n2-1))) = chirp.phase2;
       current =current + n2;
    end 
    
    end
    
    
elseif strcmp(chirp.type,'XiX_train_tpsweep')
    AM = zeros(1,n);
    FM = ones(1,n)*chirp.nu_init;
%     FM_arg = 2*pi*chirp.nu_init*t; % speedup for monochromatic pulses (avoids expensive cumtrapz)
    PM =0*AM;
    
    current = 1;
    
    % first pulse
    if isfield(chirp,'tp_initial')
    n_init = round(chirp.tp_initial/wave.dt);
    AM(current+(0:(n_init-1))) = chirp.scale_initial;
    PM(current+(0:(n_init-1))) = chirp.phase_initial;
    current =current + n_init;
    else
        n_init=0;
    end
    
    if isfield(chirp,'tp_end')
        n_end = round(chirp.tp_end/wave.dt);
    else
        n_end=0;
    end
       
    % train, start
    npulse_start = current;
    npulse_end = n-n_end-1;
    
%     initialize
    ntemp = 0;
    n1 =round((chirp.tp1(1)+diff(chirp.tp1)*ntemp/(npulse_end-npulse_start))/wave.dt);
    n2 =round((chirp.tp2(1)+diff(chirp.tp2)*ntemp/(npulse_end-npulse_start))/wave.dt);
    
    while current+n1+n2<=npulse_end
       AM(current+(0:(n1-1))) = chirp.scale1;
       PM(current+(0:(n1-1))) = chirp.phase1;
       FM(current+(0:(n1-1))) = chirp.nu_obs1;
       current =current + n1;
       ntemp =ntemp + n1;
       
       AM(current+(0:(n2-1))) = chirp.scale2;
       PM(current+(0:(n2-1))) = chirp.phase2;
       FM(current+(0:(n2-1))) = chirp.nu_obs2;
       current =current + n2;
       ntemp =ntemp + n2;
       
    n1 =round((chirp.tp1(1)+diff(chirp.tp1)*ntemp/(npulse_end-npulse_start))/wave.dt);
    n2 =round((chirp.tp2(1)+diff(chirp.tp2)*ntemp/(npulse_end-npulse_start))/wave.dt);
    end 
    
    if isfield(chirp,'tp_end')
        AM(current+(0:(n_end-1))) = chirp.scale_end;
        PM(current+(0:(n_end-1))) = chirp.phase_end;
    end
   
elseif strcmp(chirp.type,'PulsePol')
    AM = zeros(1,n);
    FM = ones(1,n)*chirp.nu_init;
    FM_arg = 2*pi*chirp.nu_init*t; % speedup for monochromatic pulses (avoids expensive cumtrapz)
    PM =0*AM;
    
    current = 1;
    
    
    n90 = round(chirp.tp90/wave.dt);
    nd = round(chirp.d/wave.dt);
    nround = (8*n90 + 4*nd) ;
    
    tp_vec = [n90 nd 2*n90 nd n90 n90 nd 2*n90 nd n90];
    pulse_vec = [1 0 1 0 1 1 0 1 0 1];
    phase_vec = [pi/2 0 pi 0 pi/2 0 0 pi/2 0 0];
    
    while (current + nround )<= n
        for ii=1:numel(tp_vec)
           AM(current:(current+tp_vec(ii)-1))=pulse_vec(ii);
           PM(current:(current+tp_vec(ii)-1))=phase_vec(ii);
           current = current +tp_vec(ii);
        end
    end
        
        
        
 
elseif strcmp(chirp.type,'RA_spinlock_sandwich')    
    AM = zeros(1,n);
    FM = ones(1,n)*chirp.nu_init;
    FM_arg = 2*pi*chirp.nu_init*t; % speedup for monochromatic pulses (avoids expensive cumtrapz)
    PM =0*AM;
    
    n_ramp =round(chirp.tramp/wave.dt);
    
    current=1;
    n_pulse =round(chirp.tp1/wave.dt);
    AM(current+(0:(n_pulse-1))) = chirp.scale1;
    PM(current+(0:(n_pulse-1))) = chirp.phi1;
    
    nsmooth = ceil(chirp.tsmooth/wave.dt);
    rel_form = normpdf(0:(nsmooth-1),0,nsmooth/3);
    rel_form = rel_form/max(rel_form);
    if n_ramp>0
        end_smooth = (chirp.a0-chirp.da);
        if end_smooth<0
            end_smooth = 0;
        end
        AM(current+(n_pulse-nsmooth)+(0:(nsmooth-1))) = end_smooth+rel_form*(chirp.scale1-end_smooth);
    else
        AM(current+(n_pulse-nsmooth)+(0:(nsmooth-1))) = chirp.scale2+rel_form*(chirp.scale1-chirp.scale2);
    end
    current=current+n_pulse;
    
    
    if n_ramp>0 && chirp.a0>0
        x = (0:(n_ramp-1))/n_ramp;
        AM_ramp = chirp.a0 - chirp.kmod*tan(2*atan(chirp.da/chirp.kmod)*(0.5-x));
        
        if any(AM_ramp>1) || any (AM_ramp<0)
            
            
            
            [~,poi]=min(abs(AM_ramp));
            
            mid = round(numel(AM_ramp)/2);
            y_left = AM_ramp(poi:mid);
            x_left = x(poi:mid);
            x_new = linspace(min(x_left),max(x_left),numel(1:mid));
            y_left = interp1(x_left,y_left,x_new);
            AM_ramp(1:mid)=y_left;
            
            mid = mid+1;
            [~,poi]=min(abs(AM_ramp-1));
            y_right = AM_ramp(mid:poi);
            x_right = x(mid:poi);
            x_new = linspace(min(x_right),max(x_right),numel(mid:numel(x)));
            y_right = interp1(x_right,y_right,x_new);
            AM_ramp(mid:end)=y_right;
            
            
        end
        AM(current+(0:(n_ramp-1))) = AM_ramp;
        PM(current+(0:(n_ramp-1))) = chirp.phi_ramp;
    end
    
    
    current=current+n_ramp;
    
    n_pulse =round(chirp.tp2/wave.dt);
    AM(current+(0:(n_pulse-1))) = chirp.scale2;
    PM(current+(0:(n_pulse-1))) = chirp.phi2;
    nsmooth = ceil(chirp.tsmooth)/wave.dt;
    rel_form = normpdf(-(nsmooth-1):0,0,nsmooth/3);
    rel_form = rel_form/max(rel_form);
    if n_ramp>0
        end_smooth = (chirp.a0+chirp.da);
        if end_smooth>1
            end_smooth = 1;
        end
        AM(current+(0:(nsmooth-1))) = end_smooth+rel_form*(chirp.scale2-end_smooth);
    end
    

elseif strcmp(chirp.type,'RA_spinlock_sandwich_2')    
    AM = zeros(1,n);
    FM = ones(1,n)*chirp.nu_init;
    FM_arg = 2*pi*chirp.nu_init*t; % speedup for monochromatic pulses (avoids expensive cumtrapz)
    PM =0*AM;
    
    n_ramp =round(chirp.tramp/wave.dt);
    
    current=1;
    n_pulse =round(chirp.tp1/wave.dt);
    AM(current+(0:(n_pulse-1))) = chirp.scale1;
    PM(current+(0:(n_pulse-1))) = chirp.phi1;
    
    nsmooth = ceil(chirp.tsmooth/wave.dt);
    rel_form = normpdf(0:(nsmooth-1),0,nsmooth/3);
    rel_form = rel_form/max(rel_form);
    if n_ramp>0
        end_smooth = (chirp.a0-chirp.da);
        if end_smooth<0
            end_smooth = 0;
        end
        AM(current+(n_pulse-nsmooth)+(0:(nsmooth-1))) = end_smooth+rel_form*(chirp.scale1-end_smooth);
    else
        AM(current+(n_pulse-nsmooth)+(0:(nsmooth-1))) = chirp.scale2+rel_form*(chirp.scale1-chirp.scale2);
    end
    current=current+n_pulse;
    
    
    if n_ramp>0 && chirp.a0>0
        x = (0:(n_ramp-1))/n_ramp;
        AM_ramp = chirp.a0 - chirp.kmod*tan(2*atan(chirp.da/chirp.kmod)*(0.5-x));
        
        if any(AM_ramp>1) || any (AM_ramp<0)
            
            
            
            [~,poi]=min(abs(AM_ramp));
            
            mid = round(numel(AM_ramp)/2);
            y_left = AM_ramp(poi:mid);
            x_left = x(poi:mid);
            x_new = linspace(min(x_left),max(x_left),numel(1:mid));
            y_left = interp1(x_left,y_left,x_new);
            AM_ramp(1:mid)=y_left;
            
            mid = mid+1;
            [~,poi]=min(abs(AM_ramp-1));
            y_right = AM_ramp(mid:poi);
            x_right = x(mid:poi);
            x_new = linspace(min(x_right),max(x_right),numel(mid:numel(x)));
            y_right = interp1(x_right,y_right,x_new);
            AM_ramp(mid:end)=y_right;
            
            
        end
        AM(current+(0:(n_ramp-1))) = AM_ramp;
        PM(current+(0:(n_ramp-1))) = chirp.phi_ramp;
    end
    
    
    current=current+n_ramp;
    
    n_pulse =round(chirp.tp2/wave.dt);
    AM(current+(0:(n_pulse-1))) = chirp.scale2;
    PM(current+(0:(n_pulse-1))) = chirp.phi2;
    nsmooth = ceil(chirp.tsmooth)/wave.dt;
    rel_form = normpdf(-(nsmooth-1):0,0,nsmooth/3);
    rel_form = rel_form/max(rel_form);
    if n_ramp>0
        end_smooth = (chirp.a0+chirp.da);
        if end_smooth>1
            end_smooth = 1;
        end
        AM(current+(0:(nsmooth-1))) = end_smooth+rel_form*(chirp.scale2-end_smooth);
    end
       
   
    
    
    %% custom modulation
elseif strcmp(chirp.type,'FMAM')
    
    if isfield(chirp,'AM') && ~isempty(chirp.AM),
        AM = chirp.AM;
    else
        if ~isfield(chirp,'AM_coeff') || isempty(chirp.AM_coeff),
            warning('Event %1.0f: FM/AM wave with empty amplitude modulation parameters. Assuming constant maximum amplitude.', 0);
            chirp.AM_coeff = [1,1];
        end;
        x = linspace(0,1,length(chirp.AM_coeff));
        x0 = linspace(0,1,length(t));
        AM = interp1(x,chirp.AM_coeff,x0,'pchip');
    end;
    
    if isfield(chirp,'FM') && ~isempty(chirp.FM),
        FM = chirp.FM;
    else
        if ~isfield(chirp,'FM_coeff') || isempty(chirp.FM_coeff),
            warning('Event %1.0f: FM/AM wave with empty frequency modulation parameters. Assuming linear sweep.', 0);
            chirp.FM_coeff = [chirp.nu_init,chirp.nu_final];
        end;
        x = linspace(0,1,length(chirp.FM_coeff));
        x0 = linspace(0,1,length(t));
        FM = interp1(x,chirp.FM_coeff,x0,'pchip');
    end;
    
    % PM phase modulation (phase-shift put on top of FM)
    PM = zeros(1,n);
        
    % resonator compensation for linear sweeps: adapt FM and AM
    % this may cause trouble if critical adiabaticity is not
    % constant
    if resonatorcomp
        % approximation of time-domain amplitude modulation by resonator
        nu1_t = interp1(chirp.resonator.range,abs(chirp.resonator.nu1),FM,'pchip');
        % net AM: digital AM and AM by resonator
        nu1_t = nu1_t .* AM; FM_orig = FM;
        
        % adiabaticity: negative for downsweeps!
        wave.qref = 2*pi*t(end) / trapz(FM,1./nu1_t.^2);
        if nu1_MHz
            wave.qref = wave.qref * 0.001^2; % convert nu1 in MHz to GHz
        end
        
        if ~isinf(wave.qref) % infinity is reached for nu_init == nu_final
            % time spent per frequency
            dt_df = wave.qref./(2*pi*nu1_t.^2);
            % time t(f) from dt/df
            t_f = cumtrapz(FM,dt_df);
            % reparametrize t(f) to FM f(t)
            FM = interp1(t_f,FM,t,'pchip');
            
            % reparametrize digital AM to new FM
            AM = interp1(FM_orig,AM,FM,'pchip');
        end
        
        % keep the feature of adding resonator phase onto pulse, even
        % though it is currently used rather rarely
        if ~isreal(chirp.resonator.nu1(1))
            PM = PM + interp1(chirp.resonator.range,angle(chirp.resonator.nu1),FM,'pchip');
        end
    end
    
else
    error(['Fatal Error: Could not determine type ''' chirp.type '''! Did you send your pulse to pulse_standardize?']);
end

% nonlinearity correction of AM
if isfield(chirp,'nl_pol')
    AM = polyval(chirp.nl_pol,AM*chirp.scale); % correction at chirp.scale amplitude
    AM = AM/polyval(chirp.nl_pol,chirp.scale); % re-normalize to the intended range of AM
    % Note that for AM that was not bound to [0...1], there could be
    % problems. Such things might happen for FMAM pulses or for 
    % t_rise > t_p/2
end

% jumps in phase and amplitude, i.e. offsets to PM and multiplicators for
% AM (for instance composite pulses by pha_jump and a quick way to BIR1
% pulses by sca_jump to zero, i.e. sca_jump = [0.5 0];
if isfield(chirp,'sca_jump')
    % relative times [0...1] when the scale jump happens
    jtimes = chirp.sca_jump(:,1);
    jidx = round(jtimes*chirp.tp/wave.dt);
    
    % values to set
    jvals = chirp.sca_jump(:,2);
    
    % set the AM multipliers
    n_jumps = length(jtimes);
    for ii = 1:n_jumps
        % get the proper indices
        if ii == n_jumps
            set_range = jidx(ii):length(t);
        else
            set_range = jidx(ii):jidx(ii+1)-1;
        end
        
        AM(set_range) = AM(set_range) * jvals(ii);
    end
end
if isfield(chirp,'pha_jump')
    % relative times [0...1] when the scale jump happens
    jtimes = chirp.pha_jump(:,1);
    jidx = round(jtimes*chirp.tp/wave.dt);
    
    % values to set
    jvals = chirp.pha_jump(:,2);
    
    % set the PM offset
    n_jumps = length(jtimes);
    for ii = 1:n_jumps
        % get the proper indices
        if ii == n_jumps
            set_range = jidx(ii):length(t);
        else
            set_range = jidx(ii):jidx(ii+1)-1;
        end
        
        PM(set_range) = PM(set_range) + jvals(ii);
    end
end

% write back the modulations to wave
wave.AM = AM;
wave.FM = FM;
wave.PM = PM;

% calculate cos/sin argument
if isempty(FM_arg)
    FM_arg = 2*pi*cumtrapz(FM)*wave.dt;
end
pha_arg = FM_arg + PM;

% caculate the waveforms as required for doing an experiment, i.e. with the
% Iscale,Qscale and Qphase corrections for eventual IQ generation
real_wave=chirp.scale*chirp.Iscale* AM.*    cos(pha_arg+chirp.phase);
imag_wave=chirp.scale*chirp.Qscale* AM.*    sin(pha_arg+chirp.Qphase);


% Binary form: mapping of [-1 1] to [0,1,2,...,res-1]
real_binary=round((res-1)*(real_wave+1)/2);
imag_binary=round((res-1)*(imag_wave+1)/2);

if awg.channels==2,
    wave.binary=real_binary+1i*imag_binary;
else
    wave.binary=real_binary;
end;

% compute normalized with with vertical resolution of AWG
real_wave=2*(real_binary/(res-1)-0.5);
imag_wave=2*(imag_binary/(res-1)-0.5);

if awg.channels==2,
    wave.arbitrary=real_wave+1i*imag_wave;
else
    wave.arbitrary=real_wave;
end;

wave.vert_res = awg.vert_res;

end

