% function rmsd = rmsd(v1,v2)
% 
% calculates root-mean-square deviation
%
% v1 - input vector 1
% v2 - input vector 2
%
function rmsd = rmsd(v1,v2)

    rmsd = sum(sqrt(abs(v1.^2 - v2.^2)));

end