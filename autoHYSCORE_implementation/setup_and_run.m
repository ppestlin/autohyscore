function [Hys] = setup_and_run(Hys)

% Discribe this function


try
    
    % Start a timer for how long the experiment is running
    tStart = tic;   

    % Set starting Hys.b_field.value
    
    if isempty(Hys.b_field.value)
        Hys.b_field.value = round((Hys.res_frequ.value)/Hys.b_field.gyro_exp,2);
    elseif length(Hys.b_field.value) == 2
        Hys.b_field.value = round((Hys.b_field.value(1)/Hys.b_field.value(2)) * Hys.res_frequ.value,2);
    elseif isscalar(Hys.b_field.value)
        Hys.b_field.gyro_exp = (Hys.res_frequ.value)/Hys.b_field.value;
    else
        error(['Your field value was not defined to the types given in the comment of User_script or you did not enter a '...
            'gyro-value. b_vield.value can be an empty array, scalar or an array of length 2'])
    end
    
    
    % conf for this exp. Make sure conf in base is correct

    Hys.conf = evalin('base','conf');

    Hys.conf_start = Hys.conf;      % The starting conf for any safety-check
    
    %% Variables, advanced user can use these in User_script

    if ~isfield(Hys,'report_path')
        Hys.report_path = 'autoHYSCORE_report';
    end
    if ~isfield(Hys, 'report_name')
        Hys.report_name = 'Hyscore_automatic_rport';
    end
    if ~isfield(Hys.field_prov,'state')
        Hys.field_prov.state = 0;
    end
    if ~isfield(Hys.IR,'executed')
        Hys.IR.executed = 0;
    end
    if ~isfield(Hys.AMP_sweep,'state')    
        Hys.AMP_sweep.state = 0;
    end
    if ~isfield(Hys.b_field,'state')    
        Hys.b_field.state = 0;
    end
    if ~isfield(Hys,'Num_fs')    
        Hys.Num_fs = 0;             % Number of fieldsweeps executed
    end
    if ~isfield(Hys,'sufficient_fs')    
        Hys.sufficient_fs = 0;      % If a sufficient field sweep was measured when b_field.state = 2
    end
    if ~isfield(Hys.IR,'state')    
        Hys.IR.state = 0;
    end
    if ~isfield(Hys.nut,'state')
        Hys.nut.state = 0;
    end
    if ~isfield(Hys.ESEEM2p,'state')
        Hys.ESEEM2p.state = 0;
    end
    if ~isfield(Hys.res_profile,'state')
        Hys.res_profile.state = 0;
    end
    if ~isfield(Hys.final_amp_sweeps,'state')
        Hys.final_amp_sweeps.state = 0;
    end
    if ~isfield(Hys.ESEEM3p,'state')
        Hys.ESEEM3p.state = 0;
    end
    if ~isfield(Hys.ESEEM4p,'state')
        Hys.ESEEM4p.state = 0;
    end
    
    % a hard inversion pulse at nu_obs
    
    if ~isfield(Hys,'p_hard')
        Hys.p_hard.tp = 1*1.111111111; %15.56; %
        Hys.p_hard.nu_init = Hys.nu_obs.value;
        Hys.p_hard.scale = 1.0;
    end
    
    Hys.store_avgs.value = 0;
    
    Hys.det_len.value = 512;

    Hys.avgs.value = 1.0;

    reptime_min = Hys.myreptime.min;        % This is is needed due to a mistake with changed_fields
                                            % Should be replaced with a
                                            % propper calculation in the
                                            % future
    
    %% Check temperature
                                           
    temp_size = size(Hys.temp.try_values);                                        
    if ~isfield(Hys.temp,'value')
        error('Please provide a temperature in temp.try_values. This can be scalar or an array with several values (in K)')
    elseif isscalar(Hys.temp.try_values)
        temp_len = 1;
    elseif length(temp_size) == 2 && (temp_size(1) == 1 || temp_size(2) == 1)
        temp_len = length(Hys.temp.try_values);
    else
        error('The temp.try_values you provided was not in the form necessary. It can be scalar or an array with several values (in K)')
    end
    
    Hys.for_temp.Hys_per_temp = cell(temp_len,1);
    Hys.for_temp.temp_dir_names = cell(temp_len,1);
    Hys.for_temp.Hys_4p_snr = zeros(temp_len,1);
    

    Hys_strt = Hys;                         % Save Hys to start with for each temp

    %% Temperature loop

    for Temp_ind = 1:temp_len                                       
        
        % Make and set directory for this temp. Save the main folder
        % created by user in main_dir
        Hys_strt.for_temp.temp_dir_names{Temp_ind} = ['./' num2str(Hys.temp.try_values(Temp_ind)) 'K_tuning_' Hys.sample];
        mkdir(Hys_strt.for_temp.temp_dir_names{Temp_ind})
        main_dir = cd(Hys.for_temp.temp_dir_names{Temp_ind});
        Hys = Hys_strt;                                     % Reestablish starting Hys
        Hys.temp.value = Hys.temp.try_values(Temp_ind);
            
        % Set temp, in work

        place_holder = temperatue_interface('set',Hys.temp.value);
        
        if place_holder == -1
            save('Hyscore_structure_before_error.mat','Hys')
            error('Temperature setting error')
        end

        Hys.files_and_dirs.file_field_sweep = cell(10,1);

        if Hys.field_prov.state ==0

            pulse = [16,32];
            [Hys.b_field.gyro_exp,Hys.b_field.value, ~] = prov_field_sweep(Hys, pulse);
            Hys.b_field.value = round(Hys.b_field.value,2);
        end



        % Loop for tuning of parameters and ensuring not to many runs
        % When implementing a maximum time clock, start here

        runs = 0;
        sufficient = 0;

        while sufficient == 0 && runs < 5

            runs = runs + 1;


            % AMP sweep for pi/2 16, pi 32

            if Hys.AMP_sweep.state == 0

                pulse = [16,32];

                [scale_16_32, Hys.VideoGain.value, ~, changed_fields] = AMP_sweep(Hys, pulse, Hys.clear_files.AMP_sweep );

                changed_f_list = fieldnames(changed_fields);
                for i = 1:length(changed_f_list)
                    Hys.(changed_f_list{i}) = changed_fields.(changed_f_list{i});
                end

                Hys.p_pi2_16.scale = scale_16_32;
                Hys.p_pi_32.scale = scale_16_32;
                Hys.myreptime.min = reptime_min;        % This is is needed due to a mistake with changed_fields
            end


            % Fine field sweep to find exact field for measurement

            if Hys.b_field.state == 0

                pulse = [16,32];
                [gyro_exp,clipping,Hys.HYSCORE.field_vec,file_field_sweep,~] = field_sweep(Hys, pulse);

                Hys.Num_fs = Hys.Num_fs + 1;
                Hys.files_and_dirs.file_field_sweep{Hys.Num_fs,1} = file_field_sweep;

                if abs(Hys.b_field.value - (Hys.res_frequ.value)/Hys.b_field.gyro_exp) > 1 
                    Hys.IR.state = 0;
                    Hys.ESEEM2p.state = 0;
                end

                Hys.b_field.gyro_exp = gyro_exp;
                Hys.b_field.value = round((Hys.myLO.value+Hys.nu_obs.value)/Hys.b_field.gyro_exp,2);

                if clipping
                    Hys.b_field.state = 0;
                    Hys.AMP_sweep.state = 0;
                    continue
                end
            elseif Hys.b_field.state == 2 && Hys.sufficient_fs == 0
                pulse = [16,32];
                [~,clipping,~,file_field_sweep,~] = field_sweep(Hys, pulse);

                Hys.Num_fs = Hys.Num_fs + 1;
                Hys.files_and_dirs.file_field_sweep{Hys.Num_fs,1} = file_field_sweep;

                if clipping
                    Hys.AMP_sweep.state = 0;
                    continue
                else
                    Hys.sufficient_fs = 1;
                end
            end


            % Nutation experiment to determine length of a hard inversion pulse at nu_obs

            if Hys.nut.state == 0

                pulse_fields = [Hys.p_hard,Hys.p_pi2_16,Hys.p_pi_32];

                [Hard_length,file_nut] = nutation(Hys,pulse_fields, Hys.clear_files.nut);

                Hys.files_and_dirs.file_nut = file_nut;
                Hys.p_hard.tp = Hard_length;
                Hys.nut.state = 1;

            end


            % Inversion recover experiment to find reptime

            if Hys.IR.state == 0

                pulse_fields = [Hys.p_hard,Hys.p_pi2_16,Hys.p_pi_32]; 

                [reptime,file_IR] = inversion_recovery(Hys, pulse_fields, Hys.clear_files.IR);

                Hys.files_and_dirs.file_IR = file_IR;
                Hys.myreptime.value = reptime;
                if reptime < Hys.myreptime.min
                        Hys.myreptime.value = Hys.myreptime.min;
                end
                Hys.IR.executed = 1;
                Hys.IR.state = 1;

            end


            % 2p ESEEM to find if tau for fieldsweep had strong modulations

            if Hys.ESEEM2p.state == 0 

                pulse_fields = [Hys.p_pi2_16,Hys.p_pi_32];
                Hys.ESEEM2p.tau = Hys.tau.b_sweep;
                Hys.ESEEM2p.det_len = 512;

                [tau_2pE, intensity_ratio, file_2pESEEM] = ESEEM_2p(Hys,pulse_fields);

                Hys.files_and_dirs.file_2pESEEM = file_2pESEEM;
                if Hys.b_field.state ~= 2

                    if Hys.tau.b_sweep < tau_2pE
                        if (1/intensity_ratio)^2/2 > 1
                            Hys.b_sweep.shots.factor =  Hys.b_sweep.shots.factor * ((1/intensity_ratio)^2)/2;
                        end
                        Hys.tau.b_sweep = tau_2pE;
                        Hys.b_field.state = 0;
                        Hys.nut.state = 0;
                        Hys.IR.state = 0;
                        continue
                    end 

                end

            end

            % res profile experiement to find opitmal frequency

            if Hys.res_profile.state == 0
                pulse_fields = [Hys.p_pi2_16, Hys.p_pi2_16,Hys.p_pi_32];

                [fc,~,file_res_profile] = res_profile_exp(Hys,pulse_fields);


                Hys.files_and_dirs.file_res_profile = file_res_profile;
                if abs(fc - Hys.res_frequ.value) > 0.01 
                    Hys.b_field.value = round(Hys.b_field.value/Hys.res_frequ.value * fc,2);
                    Hys.res_frequ.value = fc;
                    Hys.nu_obs.value = Hys.res_frequ.value - Hys.myLO.value;
                    if Hys.b_field.state ~= 2
                        Hys.b_field.state = 0;
                    end
                    Hys.AMP_sweep.state = 0;
                    Hys.ESEEM_2p.state = 0;
                    Hys.IR.state = 0;
                    Hys.nut.state = 0;
                    Hys.res_profile.state = 1;
                    continue
                else
                    Hys.b_field.value = round(Hys.b_field.value/Hys.res_frequ.value * fc,2);
                    Hys.res.frequ.value = fc;
                    Hys.nu_obs.value = Hys.res_frequ.value - Hys.myLO.value;
                    Hys.res_profile.state = 1;
                end
            end
            sufficient = 1;
        end

        if (sufficient == 0 && runs == 5) || Hys.myreptime.value > 10e6
            save('Hyscore_structure_before_error.mat','Hys')
            error('The loop did not converge or reptime was to large')
        end
        % Check AMP sweep for pi/2 16, pi 32 one more time

        if Hys.final_amp_sweeps.state == 0
            pulse = [16,32];

            [scale_16_32, Hys.VideoGain.value, file_amps_16_32, ~] = AMP_sweep(Hys, pulse, Hys.clear_files.AMP_sweep );

            Hys.files_and_dirs.file_amps_16_32 = file_amps_16_32;
            Hys.p_pi2_16.scale = scale_16_32;
            Hys.p_pi_32.scale = scale_16_32;


            % AMP sweep for pi/2 8, pi 16

            pulse = [8,16];

            [scale_8_16, Hys.VideoGain.value, file_amps_8_16, changed_fields] = AMP_sweep(Hys, pulse, Hys.clear_files.AMP_sweep );

            Hys.files_and_dirs.file_amps_8_16 = file_amps_8_16;
            changed_f_list = fieldnames(changed_fields);
            for i = 1:length(changed_f_list)
                Hys.(changed_f_list{i}) = changed_fields.(changed_f_list{i});
            end

            Hys.p_pi2_8.scale = scale_8_16;
            Hys.p_pi_16.scale = scale_8_16;
        end

        % 3pESEEm to find best tau values for HYSCORE

        if Hys.ESEEM3p.state == 0
            pulse_fields = [Hys.p_pi2_16, Hys.p_pi2_16, Hys.p_pi2_16];
            Hys.ESEEM3p.det_len = 512;

            [taus, conf_3pESEEM,dir_3pESEEM] = ESEEM_3p(Hys, pulse_fields);

            Hys.files_and_dirs.dir_3pESEEM = dir_3pESEEM;
            Hys.conf_3p = conf_3pESEEM;
            Hys.taus = taus;
            Hys.taus_found = taus; % If we dont measure one of these taus we still keep it in Hys
            Hys.ESEEM3p.state = 1;
        end

        % 4pESEEM to determine final reptime

        if Hys.ESEEM4p.state == 0
            pulse_fields = [Hys.p_pi2_16, Hys.p_pi2_16, Hys.p_pi_16, Hys.p_pi2_16];
            Hys.ESEEM4p.tau = Hys.taus(1);
            Hys.ESEEM4p.T1 = Hys.taus(1); % + 8;
            Hys.ESEEM4p.det_len = 512;

            [final_reptime, final_shots,signal_to_noise,signal_to_noise_per_time, conf_4pESEEM,dir_4pESEEM] = ESEEM_4p(Hys, pulse_fields);

            Hys.files_and_dirs.dir_4pESEEM = dir_4pESEEM;
            Hys.ESEEM4p.final_shots = round(final_shots);
            Hys.conf_4p = conf_4pESEEM;
            Hys.myreptime.value = final_reptime;
            Hys.ESEEM4p.signal_to_noise = signal_to_noise;
            Hys.ESEEM4p.signal_to_noise_per_time = signal_to_noise_per_time;
            Hys.ESEEM4p.state = 1;
        end
    
        save(['Hys_' num2str(Hys.temp.try_values(Temp_ind)) '.mat'],'Hys')
        Hys_strt.for_temp.Hys_per_temp{Temp_ind} = Hys;
        Hys_strt.for_temp.Hys_4p_snr(Temp_ind) = Hys.ESEEM4p.signal_to_noise;
        cd(main_dir)

    end

    %% Finding best temperature

    [~,Hys_ind] = max(Hys_strt.for_temp.Hys_4p_snr);
    Hys = Hys.for_temp.Hys_per_temp{Hys_ind};
    Hys.for_temp.Hys_per_temp = Hys_strt.for_temp.Hys_per_temp;
    Hys.for_temp.Hys_4p_snr = Hys_strt.for_temp.Hys_4p_snr;
    Hys.for_temp.temp_dir_names = Hys_strt.for_temp.temp_dir_names;
    
    Hys.temp.value = Hys.temp.try_values(Hys_ind);

    %% Set best temp, in work

    place_holder = temperatue_interface('set',Hys.temp.value);
        
    if place_holder == -1
        save('Hyscore_structure_before_error.mat','Hys')
        error('Temperature setting error')
    end
    

    %% Prepare and start HYSCORE

    %%%%%% Calculate time here to get shots or 

    if ~isfield(Hys.HYSCORE,'field_vec') 
        Hys.HYSCORE.field_vec = Hys.b_field.value;
    end
    
    t_before_Hyscore = toc(tStart); % s
    t_before_Hyscore = round(t_before_Hyscore);
    t_before_Hyscore = t_before_Hyscore/(60*60);    % hrs

    time_4all_Hyscores = (Hys.max_runtime - t_before_Hyscore)*0.98;  % The 0.98 are to leave some space for calc error

    trace_time =  Hys.ESEEM4p.final_shots * Hys.HYSCORE.T2_swp.dim * 8 * Hys.myreptime.value/(1e9*60*60);
    Hyscore_time = Hys.HYSCORE.T2_swp.dim * trace_time;
    time_all = Hyscore_time * length(Hys.taus) * length(Hys.HYSCORE.field_vec);
    n = 0;
    while time_4all_Hyscores < time_all
        n = n + 1;
        time_all = Hyscore_time * (length(Hys.taus)-n) * length(Hys.HYSCORE.field_vec);  
    end
    
    if length(Hys.taus)-n < 1
        error('Measurement would go to long')
    elseif n>0
        Hys.taus = zeros(length(Hys.taus_found)-n,1);
        for j = 1:(length(Hys.taus_found)-n)
            Hys.taus(j) = Hys.taus_found(j);
        end
    end
    
    if ~Hys.finish_early
       
        Hys.HYSCORE.shots.value = max(floor( Hys.ESEEM4p.final_shots * time_4all_Hyscores/time_all),Hys.HYSCORE.min_shots);
        
    else
        Hys.HYSCORE.shots.value =  Hys.ESEEM4p.final_shots;
    end

    pulse_fields = [Hys.p_pi2_16, Hys.p_pi2_16, Hys.p_pi_16, Hys.p_pi2_16];

    [is_finished] = HYSCORE_4p(Hys, pulse_fields);
    
    
    
    if is_finished
        disp('The run is finished. A final sorted Hyscore structure was saved to "Hyscore_structure_at_finish.mat".')
        [~, neworder] = sort(lower(fieldnames(Hys)));
        Hys = orderfields(Hys, neworder);
        save('Hyscore_structure_at_finish.mat','Hys')
    end


catch ERROR
    [~, neworder] = sort(lower(fieldnames(Hys)));
    Hys = orderfields(Hys, neworder);
    save('Hyscore_structure_before_error.mat','Hys')
    warning(['It seems that the code encountered an error. If this error' ...
        'is due to an error in the code, please contact the system admin.'...
        'You can find the current Hyscore structure in the file'...
        '"Hyscore_structure_before_error.mat" in your current folder'])
    rethrow(ERROR)
end

end




