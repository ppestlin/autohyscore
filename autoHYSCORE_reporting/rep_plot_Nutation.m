function rep_plot_Nutation(reportdata)

    file = reportdata.files_and_dirs.file_nut;
    
    clear options;
    options.plot = 0; % do not plot by uwb_eval
    options.evlen = 256; % enforce a 256 point evaluation window
    
    options.phase_all = 0; %0 for nutation, 1 for ampsweeps;
    
    cut=1;
    
    dta_cont = cell(length(file),1);
    dta_x_cont = dta_cont;
    ev_coll = dta_cont;
    exp_cont = dta_cont;
    
    
    output = uwb_eval2(file,options);
    
    dta_ev = output.dta_ev; % echo integrals
    
    ev_coll = dta_ev;
    dta_x_cont = output.dta_x{1}; % axis of first indirect dimension
    
    
    figure('Visible','off','Position', [400,400,300,225]); clf; hold on; 
    
    starti = 1;
    
    plot(dta_x_cont(starti:end),real(ev_coll(starti:end))/max(real(ev_coll(starti:end))),'-','Color','r')
    ylabel('Re(Echo integral) [norm.]')
    xlabel('Sweep axis')
    [pks,x] = findpeaks(-real(ev_coll(cut:end))/max(real(ev_coll(cut:end))));
    plot(dta_x_cont(x(1)),-pks(1),'o','MarkerSize',5,'Color','blue')
    legend('Data', 'Result', 'location', 'best')


end