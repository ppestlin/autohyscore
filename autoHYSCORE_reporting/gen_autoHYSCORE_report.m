function gen_autoHYSCORE_report(Hys)

% reportdata = gen_reportdata(Hysc)
%
% Create Hyscore report 
% Hys input is Hyscore experimental structure
%

% Hys = struct; % initalize empty struct

if ~isfield(Hys, 'report_path')
    Hys.report_path = pwd;   
end
if ~isfield(Hys, 'report_name')
    Hys.report_filename = 'Hyscore_automatic_rport'; %Hysc.filename;
end

Hys.EDFS_filename = '20220513_1850_2pecho_field_sweep.mat';


%Send structure to workspace
assignin('base', 'reportdata', Hys);
%Generate report
report autoHYSCORE_report_v1 -fpdf;
%Remove structure from workspace
evalin('base', 'clear reportdata');

end
