function rep_plot_ESEEM4p(reportdata)

    
    bsl_corrected = 'max intensity';
    
    Trace_time = 1000;
    points = 512;
    clear snr_per_time;

    figure('Visible','off','Position',[400, 400, 650, 225])

    clear contents;
    directory = ['./' reportdata.files_and_dirs.dir_4pESEEM];
    contents = dir(directory);
    
    for i = 1:(length(contents))
        str_here = strsplit(contents(i).name,'_');
        if length(str_here) > 1
            contents(i).rep_time = str2double(str_here{end}(1:end-6));    
        end
    end
    % Sort data
    tab = struct2table(contents(3:end)); % removes also first 2 entries
    tab_sorted = sortrows(tab, 'rep_time','descend');
    contents = table2struct(tab_sorted);
    contents_len = length(contents);
    
    % options for uwb_eval
    clear options;
    options.plot = 0; % do not plot by uwb_eval
%     options.evlen = 256; %128; %56; % enforce a 256 point evaluation window
    options.phase_all = 0; %0 for nutation, 1 for ampsweeps;
    
    % Initialize variables and figures
    dta_cont = cell(contents_len,1);
    dta_x_cont = dta_cont;
    ev_coll = dta_cont;
    exp_cont = dta_cont;
    delta = dta_cont;
    
    % Plot raw data
    
    for i=1:contents_len
        file = contents(i).name;
        output = uwb_eval2([directory '/' file],options);
        dta_cont{i} = output.dta_avg; % averaged echo transients
        dta_ev = output.dta_ev(1:end); % echo integrals
        ev_coll{i} = dta_ev;
        dta_x_cont{i} = output.dta_x{1}(1:end); % axis of first indirect dimension
        t_ax = output.t_ax;
        exp_cont{i} = output.exp;
    end
    
    cmap =hsv(length(contents)+1)*0.7; % For the colors in the plot :)
    % f = figure('Name','raw data'); clf;
    subplot(1, 2, 1); hold on;
    
    for i = 1:length(contents)
        plot(dta_x_cont{i}(1:end),real(ev_coll{i}(1:end)),'-',...
        'Color',cmap(i,:))
    end
    leg_cell = cell(contents_len,1);
    for i = 1:contents_len
        leg_cell{i}= sprintf('Reptime %.2f ms',contents(i).rep_time);
    end
    legend(leg_cell,'Location','best')
    ylabel('Re( Echo integral ) [arb]')
    xlabel('Sweep axis in indices (T1)')
    title('Raw data 4pESEEM')
    
    
    for i = 1:contents_len
        file_data = load([directory '/' contents(i).name]);
        myreptime = file_data.chirper.reptime;
        shots = file_data.chirper.shots;
        points = 400;
        measure_time = shots*points*8*myreptime/(1e9*60); %min
        % renaming variables
        
        t=(dta_x_cont{i});
        vexp{i}=transpose(real(ev_coll{i}));
        
        % baseline fitting
        
        BGstart = 1;
        BGtype = 'poly';
        
        switch BGtype
            case 'poly'
                [p,s]=polyfit(t(BGstart:end),vexp{i}(BGstart:end),1); % fit the baseline by a high-order polynomial
                [bsl,delta{i}]=polyval(p,t,s); % compute the optimum baseline
            case 'sexp'
                tbg = t(BGstart:end)';
                vexpf = vexp{i}(BGstart:end)';
                fun = @(vec,tbg,vexpf) sum(sqrt((vexpf - (vec(1).*exp(-(tbg./vec(2)).^vec(3))+vec(4))).^2)); % RMSD(data-baseline)
                fun2 = @(vec)fun(vec,tbg,vexpf);
                options = optimset('MaxFunEvals',1e8,'MaxIter',1e6);
                vec = fminsearch(fun2,[vexpf(1) 100 1 vexpf(end)],options);
                bsl = vec(1).*exp(-(t./vec(2)).^vec(3))+vec(4); % compute the optimum baseline
            case 'bisexp'
                tbg = t(BGstart:end)';
                vexpf = vexp{i}(BGstart:end)';
                fun = @(vec,tbg,vexpf) sum(sqrt((vexpf - (vec(1).*exp(-(tbg./vec(2)).^vec(3)) + vec(4).*exp(-(tbg./vec(5)).^vec(6)) + vec(7))).^2)); % RMSD(data-baseline)
                fun2 = @(vec)fun(vec,tbg,vexpf);
                options = optimset('MaxFunEvals',1e8,'MaxIter',1e6);
                vec = fminsearch(fun2,[vexpf(1) 10 1 vexpf(1)./2 100 1 vexpf(end)],options);
                bsl = vec(1).*exp(-(t./vec(2)).^vec(3)) + vec(4).*exp(-(t./vec(5)).^vec(6)) + vec(7); % compute the optimum baseline
            otherwise
                % Use 'sexp' as standard to make sure the code does not stop
                tbg = t(BGstart:end)';
                vexpf = vexp{i}(BGstart:end)';
                fun = @(vec,tbg,vexpf) sum(sqrt((vexpf - (vec(1).*exp(-(tbg./vec(2)).^vec(3))+vec(4))).^2)); % RMSD(data-baseline)
                fun2 = @(vec)fun(vec,tbg,vexpf);
                options = optimset('MaxFunEvals',1e8,'MaxIter',1e6);
                vec = fminsearch(fun2,[vexpf(1) 100 1 vexpf(end)],options);
                bsl = vec(1).*exp(-(t./vec(2)).^vec(3))+vec(4); % compute the optimum baseline
        end
%         figure(400); clf; % in Figure 2, the baseline correction is shown
%         plot(t,vexp{i},'k'); % original data are plotted BLACK
%         hold on;
%         plot(t,bsl,'r'); % fitted baseline (decay function) is plotted RED
%     %     plot(t,vexp-bsl,'b'); % baseline-corrected data are plotted BLUE
%         plot([t(BGstart) t(BGstart)],[min((vexp{i}-bsl)) max(vexp{i})],'k:'); % dotted line denotes start of background fit
%         ylabel('Re( Echo integral ) [arb]')
%         xlabel('Sweep axis in indices (T1)')
%         title('Baseline corrected 4pESEEM')
        
        switch bsl_corrected
            case 'mod depth'
                [vexp_up, vexp_low] = envelope(vexp{i}-bsl,7, 'peak');
                signal = vexp_up(1)-vexp_low(1);
                noise = std(vexp{i}(0.9*length(vexp):end)-bsl(0.9*length(vexp{i}):end));
                snr_per_time(i) = signal/(noise*measure_time);
                snr(i) = signal/noise;
                figure(400 + i); clf; hold on;
                plot(t,vexp{i}-bsl,'g')
    %             plot(t,bsl,'r')
                plot(t,vexp_low,'b')
                plot(t,vexp_up,'b')
            
            case 'max intensity'
%                 figure(400 + i); clf;
%                 envelope(vexp{i},10,'peak');
%                 ylabel('Re( Echo integral ) [arb]')
%                 xlabel('Sweep axis in indices (T1)')
                noise = std(vexp{i}(round(0.9*length(vexp{i})):end));
                if i == 1
                    [max_val,max_ind] = max(vexp{i});
                    max_up = max_ind + 1;
                    max_low = max_ind - 1;
                    if max_low < 1
                        max_low = 1;
                    end
                end
                signal = mean(vexp{i}(max_low:max_up));
                
                snr_per_time(i) = signal/(noise*sqrt(measure_time));
                snr(i) = signal/noise;
               
                
            otherwise
                error('Wrong input for analyzation. Check bsl_corrected!')
        
        end 
    end
    if snr_per_time(contents_len) < snr_per_time(1)* 0.9|| snr_per_time(contents_len) < snr_per_time(contents_len-1)*0.9
        signal_to_noise = snr(contents_len-1);
        signal_to_noise_per_time = snr_per_time(contents_len-1);
        
        subplot(1,2,2); hold on;
        plot(vexp{contents_len-1})
        ylabel('Re(Echo integral) [arb]')
        xlabel('Sweep axis in indices (T1)')
        title('4pESEEM trace of result')
    else
        signal_to_noise = snr(contents_len);
        signal_to_noise_per_time = snr_per_time(contents_len);
        
        subplot(1,2,2); hold on;
        plot(vexp{contents_len})
        ylabel('Re( Echo integral ) [arb]')
        xlabel('Sweep axis in indices (T1)')
        title('Trace of result')
    end   

end