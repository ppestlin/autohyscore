function rep_plot_IR(reportdata)

file = reportdata.files_and_dirs.file_IR;

    % Options for uwb_eval
    options.evlen = 256;
    options.phase_all = 1;
    options.plot = 0;

    % Variablen initialisieren
    dta_cont = cell(length(file),1);
    dta_x_cont = dta_cont;
    ev_coll = dta_cont;
    exp_cont = dta_cont;

    output = uwb_eval2(file,options);

    dta_cont = output.dta_avg; % averaged echo transients
    dta_ev = output.dta_ev; % echo integrals

    ev_coll = dta_ev;
    dta_x_cont = output.dta_x{1}; % axis of first indirect dimension
    
    % First, fit a polynomial of first degree to the last 20% of the data
    
    last_20 = round(length(dta_x_cont) * 0.8);
    [~,minimum] = min(real(ev_coll));
    
    equil_model = polyfit(dta_x_cont(last_20:end),transpose(real(ev_coll(last_20:end))/max(real(ev_coll))),1);
    
    equil_y = polyval(equil_model,dta_x_cont(minimum:end));
    
    % Plot
    figure('Visible','off', 'Position', [400, 400, 300, 225]); clf; hold on;
    hold on; 
    plot(dta_x_cont(minimum:end),real(ev_coll(minimum:end))/max(real(ev_coll)),'Color','red')
%     legend(legendvec,'Location','best')
%     set(gca,'FontSize',15);
    ylabel('Re(Echo integral) [norm.]')
    xlabel('Repitition time [ms]')
    title('Inversion recovery')
    plot(dta_x_cont(minimum:end), equil_y,'Color','blue')
    ylim([0.5,1])
    
    threshhold = 0.985;                                     % -> Updated for version 1.1, bug fix of wrong reptime
    
    if equil_model(1) * 10^7 > 0.01
        warning('Starting reptime might be too short');
    elseif equil_model(2) < 0.985 && equil_model(2) > 0.95 
        threshhold = equil_model(2);
    elseif equil_model(2) > 0.985
        threshhold = 0.985;
    elseif equil_model(2) < 0.95
        threshhold = 0.95;
    end
    
    dta_y_cont = transpose(real(ev_coll(minimum:end))/max(real(ev_coll)));
    
    exp_model = @(beta,x)(beta.a*exp(beta.b*x) + beta.c*exp(beta.d*x));
    
    beta = fit(dta_x_cont(minimum:end), dta_y_cont, 'exp2');
    
    results = exp_model(beta,transpose(dta_x_cont(minimum:end)));
    
    plot(dta_x_cont(minimum:end), results, 'Color','green');
    
    abs_y_cont = -abs(results - threshhold); % 0.985);
    
    [~ ,reptime] = findpeaks(abs_y_cont);

    reptime = reptime + minimum;
    
    reptime = dta_x_cont(reptime);
    
    plot(reptime,[exp_model(beta,reptime)],'o','MarkerSize',5,'color','Black')

    legend('Data','Equilibrium','Modell', 'Result','Location','best')


end 