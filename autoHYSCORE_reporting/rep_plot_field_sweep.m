function rep_plot_field_sweep(reportdata)
    
    

    ii = 0;

    for i = 1:length(reportdata.files_and_dirs.file_field_sweep)
        if isempty(reportdata.files_and_dirs.file_field_sweep{i,1})
            break
        else
            ii = i;
        end
    
    if ii > 1
        width = 650;
        alpha = 2;
    else
        width = 300;
        alpha = 1;
    end

    hight = ceil(i/2) * 250;

    figure('Visible','off','position',[400,400, width ,hight]);

    if ii == 0
        return
    end
    

    for fs_loop = 1:ii
    
        file = reportdata.files_and_dirs.file_field_sweep{fs_loop,1};
        
        clear options;
        % options.evlen = 1000; %Length of the window to store the echo transient and perform the integration. By default, UWB_EVAL searches the best range
        options.plot = 0; % no plots generated
        options.phase_all = 0; % phase all data points, since amplitude sweeps make no sense without phasing
        % output = uwb_eval(file,options);
        output = uwb_eval2(file,options);
        dta_x = output.dta_x{1}; % Cell of indirect dimensions of the experiment here B from B sweep
        dta_ev = output.dta_ev; % Integrated echo transients
        subplot(ceil(fs_loop/2), alpha,fs_loop); hold on;
        findpeaks(real(dta_ev)/max(real(dta_ev)),dta_x,...
            'MinPeakProminence',0.01, 'MinPeakHeight',0.1);
%         set(gca,'FontSize',15);
        xlabel('B_0 [G]')
        ylabel('echo intg [norm]')
        title(['Field sweep with peaks Nr. ' num2str(fs_loop)])
        hold off;
    end

end