function rep_plot_AMP_sweep(reportdata)


%% Prepare data

a = 0;
b = 0;

if isfield(reportdata.files_and_dirs, 'file_amps_8_16') && ~isempty(reportdata.files_and_dirs.file_amps_8_16)
    a = a + 1;
    b = 1;
end
if isfield(reportdata.files_and_dirs, 'file_amps_16_32') && ~isempty(reportdata.files_and_dirs.file_amps_16_32)
    a  = a + 1;
end

if a > 1
    width = 650;
else
    width = 300;
end

    hight = 225;

    figure('Visible','off','position',[100,100, width ,hight]);

    
for amp_loop = 1:a

    if b == 1 && amp_loop == 1
        file = reportdata.files_and_dirs.file_amps_8_16;
    else 
        file = reportdata.files_and_dirs.file_amps_16_32;
    end
    
    % Options for uwb_eval
    options.evlen = 256;
    options.phase_all = 1;
    options.plot = 0;
    
    output = uwb_eval2(file,options);
    
    dta_cont = output.dta_avg; % averaged echo transients
    dta_ev = output.dta_ev; % echo integrals
    
    ev_coll = dta_ev;
    dta_x_cont = output.dta_x{1}; % axis of first indirect dimension
    
    % Functionn to determine maximum(scale)
    
    real_ev_coll = real(ev_coll);
    
    % Fit to find a propper maximum/ the scale of the pulse
    [M_value, M_index] = max(real_ev_coll);
    x_fit = dta_x_cont((M_index - 10):(M_index + 10));
    y_fit = transpose(real_ev_coll((M_index - 10):(M_index + 10)));
    p_fit = polyfit(x_fit, y_fit, 3);
    p_der = polyder(p_fit);
    p_root = roots(p_der);
    [root_value, root_index] = min(abs(p_root - dta_x_cont(M_index)));
    root_wanted = p_root(root_index);
    
    %% Plot
    
    subplot(1,2,amp_loop); hold on;
    plot(dta_x_cont(1:1*length(dta_x_cont)),real(ev_coll(1:1*length(dta_x_cont))),'Color','blue')
    plot(x_fit, polyval(p_fit,x_fit), 'Color','red')
    plot([root_wanted],[M_value],'o-','color','green')
    legend('Data' , 'Fit', 'Result')
%     set(gca,'FontSize',20);
    ylabel('Re(Echo integral) [arb]')
    xlabel('Pulse scale')
    if b == 1 && amp_loop == 1
        title('AMP sweep 8 16 ns')
    else
        title('AMP sweep 16 32 ns')
    end
    hold off;

end

end