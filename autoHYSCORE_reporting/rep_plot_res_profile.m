function rep_plot_res_profile(reportdata)

%% Prepare data

file = reportdata.files_and_dirs.file_res_profile;
% Options for uwb_eval
options.phase_all = 0;
options.plot = 0;
options.ref_echo_2D_idx = 1; % enforce the use of the first point
% get downconverted data by uwb_eval
output = uwb_eval2(file,options);
dta_avg = output.dta_avg; % averaged echo transients
nut2d_dconv = output.dta_ev; % echo integrals
nut_x = output.dta_x{1}; % axis of first indirect dimension here length of first pulse
LO_freq = output.exp.LO;
nut_y = output.dta_x{2} + LO_freq; % axis of second indirect dimension here sweep of magnetic field
trace = real(nut2d_dconv);
trace = trace - repmat(mean(trace),size(trace,1),1);
[fdy2, fdx2] = getmultspec(trace,nut_x,64,2); % adapt to exp. points, window fct.
% there are some problematic traces with too low nu1 around zero-freq
nuoffs = find(fdx2 > 0.005,1);
% get the nutation frequency by taking the maximum
[~,nu1_id] = max(abs(fdy2(nuoffs:end,:)));
nu1 = abs(fdx2(nu1_id+nuoffs-1));
% lorentz-fit part
[fmax,fc_ind] = max(nu1*1e3);           % Manual maximum
fc = nut_y(fc_ind);                     
[hmpX, ~] = intersections(nut_y,nu1*1e3,nut_y,(fmax/2)*ones(length(nut_y),1)); % half maximum points
if isempty(hmpX)
    df = 0.1;                           % Random starting value to avoid errors
elseif size(hmpX)<2
    df = 2*abs(hmpX(1)-fc);             % Case of 1 intersection
else
    df = abs(hmpX(1) - hmpX(end));      % Case of min 2 intersections
end
beta = [df,fc];
model = @(beta,x)(fmax./(1 + (((x - beta(2))*2/beta(1))).^2));  % Lorentz model
beta = nlinfit(nut_y, nu1*1e3,model,beta);                      % Fit
result = model(beta, nut_y);
fc = beta(2);

%% Plot

figure('visible', 'off', 'Position',[400, 400, 400, 250]); clf; hold on; 
plot(nut_y,nu1*1e3,'color','blue')
xlabel('f [GHz]')
ylabel('\nu_1 [MHz]')
plot(nut_y,result,'color','red')
plot(fc,fmax,'*','MarkerSize',5,'color','Black')
title('Resonator profile')
legend('Data', 'Lorentz fit', 'Result', 'Location','best')

end