file = '20220513_1847_2pESEEM_tau_20shots';
    
modul_th = 0.25;
inten_th = 0.15;
%     envelope_window = 45;
BGtype = 'sexp';



clear options;
options.plot = 0; % do not plot by uwb_eval
%     options.evlen = 256; %128; %56; % enforce a 256 point evaluation window
options.phase_all = 0; %0 for nutation, 1 for ampsweeps;

output = uwb_eval2(file,options);

dta_ev = output.dta_ev; % echo integrals
ev_coll = dta_ev;
dta_x_cont = output.dta_x(1:end); % axis of first indirect dimension

t=(dta_x_cont{1});
vexp=transpose(real(ev_coll));

% baseline fitting

BGstart = 1;

switch BGtype
    case 'poly'
        [p,~]=polyfit(t(BGstart:end),vexp(BGstart:end),1); % fit the baseline by a high-order polynomial
        bsl=polyval(p,t); % compute the optimum baseline
    case 'sexp'
        tbg = t(BGstart:end)';
        vexpf = vexp(BGstart:end)';
        fun = @(vec,tbg,vexpf) sum(sqrt((vexpf - (vec(1).*exp(-(tbg./vec(2)).^vec(3))+vec(4))).^2)); % RMSD(data-baseline)
        fun2 = @(vec)fun(vec,tbg,vexpf);
        options = optimset('MaxFunEvals',1e8,'MaxIter',1e6);
        vec = fminsearch(fun2,[vexpf(1) 100 1 vexpf(end)],options);
        bsl = vec(1).*exp(-(t./vec(2)).^vec(3))+vec(4); % compute the optimum baseline
    case 'bisexp'
        tbg = t(BGstart:end)';
        vexpf = vexp(BGstart:end)';
        fun = @(vec,tbg,vexpf) sum(sqrt((vexpf - (vec(1).*exp(-(tbg./vec(2)).^vec(3)) + vec(4).*exp(-(tbg./vec(5)).^vec(6)) + vec(7))).^2)); % RMSD(data-baseline)
        fun2 = @(vec)fun(vec,tbg,vexpf);
        options = optimset('MaxFunEvals',1e8,'MaxIter',1e6);
        vec = fminsearch(fun2,[vexpf(1) 10 1 vexpf(1)./2 100 1 vexpf(end)],options);
        bsl = vec(1).*exp(-(t./vec(2)).^vec(3)) + vec(4).*exp(-(t./vec(5)).^vec(6)) + vec(7); % compute the optimum baseline
    otherwise
        % Standard 2pESEEM: 'sexp'
        tbg = t(BGstart:end)';
        vexpf = vexp(BGstart:end)';
        fun = @(vec,tbg,vexpf) sum(sqrt((vexpf - (vec(1).*exp(-(tbg./vec(2)).^vec(3))+vec(4))).^2)); % RMSD(data-baseline)
        fun2 = @(vec)fun(vec,tbg,vexpf);
        options = optimset('MaxFunEvals',1e8,'MaxIter',1e6);
        vec = fminsearch(fun2,[vexpf(1) 100 1 vexpf(end)],options);
        bsl = vec(1).*exp(-(t./vec(2)).^vec(3))+vec(4); % compute the optimum baseline
end

%     [vexp_up, vexp_low] = envelope(vexp,envelope_window,'peak');

%     figure('Name','2pESEEM with fit and envelope'); clf; hold on;
%     plot(t,vexp,'g')
%     plot(t,bsl,'r')
%     plot(t,vexp_low,'b')
%     plot(t,vexp_up,'b')
%     ylabel('Re( Echo integral ) [arb]')
%     xlabel('Sweep axis')

vexp_bsl_cor = (vexp - bsl)/max(bsl);

figure(200); clf; hold on;
set(gca,'FontSize',15);
plot(t(1:0.5*length(t)),vexp(1:0.5*length(t)),'b')
plot(t(1:0.5*length(t)),bsl(1:0.5*length(t)),'r')
ylabel('Re( Echo integral ) [arb]')
xlabel('Sweep axis (ns)')
title('2pESEEM with bsl')

tau = 1/2 * t(1);

% Check strength of modulation

sufficient = 0;

[~, i] = min(abs(vexp-inten_th * max(vexp)));
i = i - 1;
tau_max = t(i)/2;
j = i+10;
if j > length(t)
    j = length(t);
end
figure(201); clf; hold on;
plot(t(1:j), vexp_bsl_cor(1:j),'b')
set(gca,'FontSize',15);
ylabel('Re( Echo integral ) [norm.]')
xlabel('Sweep axis (ns)')
title('2pESEEM bsl cor [normalized]')

 while ~sufficient 
    if abs(2 * tau - t(1)) < 1
        max_dif = max(abs(vexp_bsl_cor(1:10)));
        if max_dif > modul_th
            if vec(1)*exp(-(2*(200+tau)./vec(2)).^vec(3))+vec(4) > inten_th * max(vexp)
                tau = tau + 200;
            else
                tau = tau_max;
            end
        else
            sufficient = 1;
        end
    else
        [~, i] = min(abs(t-2*tau));
         max_dif = max(abs(vexp_bsl_cor(i-10:i+10)));
        if max_dif > modul_th
            if abs(tau - tau_max) < 1
                warning('The oscilation was to strong for the parameters, Tau was set to the largest allowed value');
                sufficient = 1;
            elseif vec(1)*exp(-(2*(200+tau)./vec(2)).^vec(3))+vec(4) > inten_th * max(vexp)
                tau = tau + 200;
            else
                tau = tau_max;
            end   
        else
            sufficient = 1;
        end
    end
end