file = '20220513_1850_2pecho_field_sweep';

clear options;
% options.evlen = 1000; %Length of the window to store the echo transient and perform the integration. By default, UWB_EVAL searches the best range
options.plot = 0; % no plots generated
options.phase_all = 0; % phase all data points, since amplitude sweeps make no sense without phasing

% output = uwb_eval(file,options);
output = uwb_eval2(file,options);

dta_x = output.dta_x{1}; % Cell of indirect dimensions of the experiment here B from B sweep
dta_ev = output.dta_ev; % Integrated echo transients
LO_freq = output.exp.LO; % local oscillator frequency
det_frq = output.det_frq; % intermediate frequency

[peaks_found, peaks_ind] = findpeaks(real(dta_ev)/max(real(dta_ev)),dta_x,...
    'MinPeakProminence',0.001, 'MinPeakHeight',0.1);

figure(20); clf; hold on
findpeaks(real(dta_ev)/max(real(dta_ev)),dta_x,...
    'MinPeakProminence',0.01, 'MinPeakHeight',0.1);
set(gca,'FontSize',15);
xlabel('B_0 [G]')
ylabel('Re( Echo integral ) [norm]')
title('Exp. Field sweep with peaks')

peaks_found = peaks_found * max(real(dta_ev));

[~,max_ind_peaks_found] = max(peaks_found);

ind_vec = 1:length(peaks_found);
ind_vec = ind_vec - max_ind_peaks_found;

% Hyscore_fields = [];
% chosen_peak_hights = [];
% for i = 1:length(ind_vec)
%     if ismember(ind_vec(i),Hys.b_field.peaks)
%         Hyscore_fields = [Hyscore_fields,round(peaks_ind(i),2)];
%         chosen_peak_hights = [chosen_peak_hights,peaks_found(i)];
%     end
% end

% if isempty(Hyscore_fields)                          % If we dont find a matching peak, measure maximum. Change to measure next lowest/ highest?
                                                    % Can get very complicated
    [~,maid] = max(real(dta_ev));
    B_obs = dta_x(maid); % maximum of spectrum
    Hyscore_fields = round(B_obs,2);
% else
%     [~,maid] = max(chosen_peak_hights);
%     B_obs = Hyscore_fields(maid);
% end

gyro_exp = (LO_freq+det_frq)/B_obs; % experimental gyromagnetic ratio
% this may be used for setting B in other experiments, or just to calculate
% the field corresponding to B_obs at a different microwave frequency;



% plot the field-domain spectra
figure(21); clf; hold on; 
hold on; 
plot(dta_x,real(dta_ev)); %plot(dta_x,dta_dc(128,:),'r')
xlabel('B_0 [G]')
ylabel('echo intg')
title('Exp. Fieldswp')
axis tight;

disp('This script updated the variable "gyro_exp" for B0 calculations');